﻿using System;
using System.Collections.Generic;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.Core.Entity
{
    public class Role : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SysName { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<UserRole> UserRoleList { get; set; } = new List<UserRole>();
    }
}
