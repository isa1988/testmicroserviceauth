﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.Core.Entity
{
    public class Session : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public Guid? RoleId { get; set; }
        [ForeignKey(nameof(RoleId))]
        public UserRole UserRole { get; set; }
        public string Token { get; set; }
        public SessionStatus Status { get; set; }
        public DateTime? CloseDate { get; set; }
        public string Info { get; set; }
    }
}
