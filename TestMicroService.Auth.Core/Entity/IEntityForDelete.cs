﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.Core.Entity
{
    public interface IEntityForDelete : IEntity
    {
        Deleted IsDeleted { get; set; }
    }

    public interface IEntityForDelete<TId> : IEntityForDelete
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
