﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.Core.Entity
{
    public class UserRole : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public Guid RoleId { get; set; }
        public Role Role { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public Deleted IsDeleted { get; set; }
    }
}
