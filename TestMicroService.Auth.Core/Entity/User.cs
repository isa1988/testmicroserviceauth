﻿using System;
using System.Collections.Generic;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.Core.Entity
{
    public class User : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<UserRole> RoleList { get; set; } = new List<UserRole>();
        public List<Session> SessionList { get; set; } = new List<Session>();
    }
}
