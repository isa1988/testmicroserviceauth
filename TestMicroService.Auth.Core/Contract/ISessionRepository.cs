﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.Core.Contract
{
    public interface ISessionRepository : IRepository<Session, Guid, OrderSessionFields>
    {
        Task<Session> GetActiveByIdAsync(Guid id, ResolveOptions resolveOptions = null);

        Task<Session> GetActiveByTokenAsync(string token, ResolveOptions resolveOptions = null);

        Task CloseAllOpenSesionAsync(Guid userId);

        Task<List<Session>> GetActiveByUserListAsync(List<Guid> userListId, ResolveOptions resolveOptions = null);
    }
}
