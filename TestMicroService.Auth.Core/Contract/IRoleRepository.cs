﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.Core.Contract
{
    public interface IRoleRepository : IRepositoryForDeleted<Role, Guid, OrderRoleFields>
    {
        Task<Role> GetBySysNameAsync(string sysName);
        Task<bool> IsEqualsSysNameAsync(string sysName);
    }
}
