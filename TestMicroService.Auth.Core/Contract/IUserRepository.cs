﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.Core.Contract
{
    public interface IUserRepository : IRepositoryForDeleted<User, Guid, OrderUserFields>
    {
        Task<List<User>> GetAllFromSearchAsync(string search, PagingOrderSetting<OrderUserFields> pagingOrderSetting, ResolveOptions resolveOptions = null);
        Task<int> GetAllFromSearchCountAsync(string search);
        Task<bool> IsEqualEmailAsync(string email, string login, Guid excludingId = default(Guid));
        Task<User> GetUserAsync(string login, string password);
        Task<User> GetUserbySesion(Guid sessionId);
        string GetEncryptedPassword(string password);
    }
}
