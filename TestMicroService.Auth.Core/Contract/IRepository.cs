﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;

namespace TestMicroService.Auth.Core.Contract
{
    public interface IRepository<T, TOrder>
        where T : class, IEntity
        where TOrder : Enum
    {
        Task<T> AddAsync(T entity);
        Task<List<T>> GetAllAsync(PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null);
        Task<int> GetAllCountAsync();
        EntityEntry<T> Update(T entity);
        EntityEntry<T> Delete(T entity);
        Task SaveAsync();
    }
    public interface IRepository<T, TId, TOrder> : IRepository<T, TOrder>
        where T : class, IEntity<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        Task<T> GetByIdAsync(TId id, ResolveOptions resolveOptions = null);
    }
}
