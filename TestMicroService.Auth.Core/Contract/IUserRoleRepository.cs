﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.Core.Contract
{
    public interface IUserRoleRepository : IRepositoryForDeleted<UserRole, Guid, OrderUserRoleFields>
    {
        Task<UserRole> GetByRoleForUserAsync(Guid userId, Guid roleId, ResolveOptions resolveOption = null);
        Task<List<UserRole>> GetByUserListAsync(Guid userId, ResolveOptions resolveOption = null);
        Task<List<UserRole>> GetByUserListAsync(Guid userId, PagingOrderSetting<OrderUserRoleFields> pagingOrderSetting, ResolveOptions resolveOption = null);
        Task<int> GetByUserCountAsync(Guid userId);
    }
}
