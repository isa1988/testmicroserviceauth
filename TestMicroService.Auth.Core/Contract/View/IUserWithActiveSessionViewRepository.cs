﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestMicroService.Auth.Core.View;

namespace TestMicroService.Auth.Core.Contract.View
{
    public interface IUserWithActiveSessionViewRepository
    {
        Task<List<UserWithActiveSessionView>> GetAllUsersWithActive();
    }
}
