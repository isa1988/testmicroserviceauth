﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.Core.Contract
{
    public interface IRepositoryForDeleted<T, TOrder> : IRepository<T, TOrder>
        where T : class, IEntityForDelete
        where TOrder : Enum
    {
        Task<List<T>> GetAllAsync(SettingSelectForDelete settingSelectForDelete, PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null);
        Task<EntityEntry<T>> DeleteFromDbAsync(T entity);
    }
    public interface IRepositoryForDeleted<T, TId, TOrder> : IRepositoryForDeleted<T, TOrder>
        where T : class, IEntityForDelete<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        Task<T> GetByIdAsync(TId id, ResolveOptions resolveOptions = null);
        Task<T> GetByIdAsync(TId id, SettingSelectForDelete settingSelectForDelete, ResolveOptions resolveOptions = null);
    }
}
