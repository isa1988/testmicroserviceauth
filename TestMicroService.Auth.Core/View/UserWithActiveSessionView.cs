﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.Core.View
{
    public class UserWithActiveSessionView
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Login { get; set; }
        public Deleted IsDeleted { get; set; }
        public Guid? SessionId { get; set; }
        public string Token { get; set; }
        public SessionStatus? Status { get; set; }
        public DateTime? CloseDate { get; set; }
        public string RoleName { get; set; }
        public string SysName { get; set; }
    }
}
