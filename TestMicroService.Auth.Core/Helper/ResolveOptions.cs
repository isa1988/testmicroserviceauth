﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMicroService.Auth.Core.Helper
{
    public class ResolveOptions
    {
        public bool IsUser { get; set; }
        public bool IsRole { get; set; }

        public bool IsSessions { get; set; }
        public bool IsRoleList { get; set; }
    }
}
