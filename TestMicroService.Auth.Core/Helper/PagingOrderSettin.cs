﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.Core.Helper
{
    public class PagingOrderSetting<T>
        where T : Enum
    {
        public int StartPosition { get; set; }
        public int PageSize { get; set; }
        public bool IsOrder { get; set; }
        public T OrderField { get; set; }
        public OrderType OrderDirection { get; set; }
    }
}
