﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.InterfaceOfService;

namespace TestMicroService.Auth.Service
{
    public abstract class GeneralServiceFromDelete<TEntity, TDto, TOrder> : GeneralService<TEntity, TDto, TOrder>, IGeneralServiceFromDelete<TDto, TOrder>
        where TEntity : class, IEntityForDelete
        where TDto : class, IServiceForDeleteDto
        where TOrder : Enum
    {
        public GeneralServiceFromDelete(IMapper mapper, IRepositoryForDeleted<TEntity, TOrder> repository)
            :base(mapper, repository)
        {
            this.repositoryBaseForDeleted = repository;
        }

        protected readonly IRepositoryForDeleted<TEntity, TOrder> repositoryBaseForDeleted;
        public override async Task<PageInfoListDto<TDto, TOrder>> GetAllAsync(PagingOrderSettingDto<TOrder> orderSetting)
        {
            var orderSet = mapper.Map<PagingOrderSetting<TOrder>>(orderSetting);
            var entityList = await repositoryBaseForDeleted.GetAllAsync(orderSet);
            var dtoList = mapper.Map<List<TDto>>(entityList);
            var totalCount = await repositoryBaseForDeleted.GetAllCountAsync();
            var dto = GetPageInfoList(dtoList, totalCount, orderSetting);
            return dto;
        }

        public override async Task<PageInfoListDto<TDto, TOrder>> GetAllDetailsAsync(PagingOrderSettingDto<TOrder> orderSetting)
        {
            var orderSet = mapper.Map<PagingOrderSetting<TOrder>>(orderSetting);
            var entityList = await repositoryBaseForDeleted.GetAllAsync(orderSet, GetOptionsForDetails());
            var dtoList = mapper.Map<List<TDto>>(entityList);
            var totalCount = await repositoryBaseForDeleted.GetAllCountAsync();
            var dto = GetPageInfoList(dtoList, totalCount, orderSetting);
            return dto;
        }
    }

    public abstract class GeneralServiceFromDelete<TEntity, TDto, TId, TOrder> : GeneralServiceFromDelete<TEntity, TDto, TOrder>, IGeneralServiceFromDelete<TDto, TId, TOrder>
        where TEntity : class, IEntityForDelete<TId>
        where TDto : class, IServiceForDeleteDto<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        public GeneralServiceFromDelete(IMapper mapper, IRepositoryForDeleted<TEntity, TId, TOrder> repository) 
            : base(mapper, repository)
        {
            repositoryBaseId = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        protected readonly IRepositoryForDeleted<TEntity, TId, TOrder> repositoryBaseId;

        protected abstract Task<string> CkeckBeforeDeleteAsync(TEntity entity);

        public virtual async Task<EntityOperationResult<TDto>> GetByIdAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id);
                var dto = mapper.Map<TDto>(entity);
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure().AddError(ex.Message);
            }
        }

        public virtual async Task<EntityOperationResult<TDto>> GetByIdDeteilsAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id, GetOptionsForDetails());
                var dto = mapper.Map<TDto>(entity);
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure().AddError(ex.Message);
            }
        }

        public virtual async Task<EntityOperationResult<TDto>> DeleteItemAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id);

                string error = await CkeckBeforeDeleteAsync(entity);
                if (!string.IsNullOrEmpty(error))
                {
                    return EntityOperationResult<TDto>.Failure().AddError(error);
                }

                var dto = mapper.Map<TDto>(entity);
                repositoryBaseId.Delete(entity);
                await repositoryBaseId.SaveAsync();
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure().AddError(ex.Message);
            }
        }

        public virtual async Task<EntityOperationResult<TDto>> DeleteItemFromDbAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id);

                string error = await CkeckBeforeDeleteAsync(entity);
                if (!string.IsNullOrEmpty(error))
                {
                    return EntityOperationResult<TDto>.Failure().AddError(error);
                }

                var dto = mapper.Map<TDto>(entity);
                await repositoryBaseId.DeleteFromDbAsync(entity);
                await repositoryBaseId.SaveAsync();
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure().AddError(ex.Message);
            }
        }
    }

    public abstract class GeneralServiceForDeleteGuid<TEntity, TDto, TOrder> : GeneralServiceFromDelete<TEntity, TDto, Guid, TOrder>,
        IGeneralServiceFromDelete<TDto, Guid, TOrder>
        where TEntity : class, IEntityForDelete<Guid>
        where TDto : class, IServiceForDeleteDto<Guid>
        where TOrder : Enum
    {
        public GeneralServiceForDeleteGuid(IMapper mapper, IRepositoryForDeleted<TEntity, Guid, TOrder> repository)
            : base(mapper, repository)
        {
        }
    }
}
