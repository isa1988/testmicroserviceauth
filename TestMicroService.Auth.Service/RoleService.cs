﻿using System.Threading.Tasks;
using AutoMapper;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.InterfaceOfService.Contract;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.Service
{
    public class RoleService : GeneralServiceForDeleteGuid<Role, RoleDto, OrderRoleFields>, IRoleService
    {
        public RoleService(IMapper mapper, IRoleRepository repository)
            : base(mapper, repository)
        {
            roleRepository = repository;
        }

        private readonly IRoleRepository roleRepository;

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions();
        }

        protected override async Task<string> CheckBeforeModificationAsync(RoleDto value, bool isNew = true)
        {
            return string.Empty;
        }

        protected override async Task<string> CkeckBeforeDeleteAsync(Role entity)
        {
            return string.Empty;
        }
    }
}
