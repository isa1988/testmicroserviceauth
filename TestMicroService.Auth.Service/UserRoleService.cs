﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.InterfaceOfService;
using TestMicroService.Auth.InterfaceOfService.Contract;
using TestMicroService.Auth.InterfaceOfService.Filters;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.Service
{
    public class UserRoleService : GeneralServiceForDeleteGuid<UserRole, UserRoleDto, OrderUserRoleFields>, IUserRoleService
    {
        public UserRoleService(IMapper mapper, IUserRoleRepository repository) 
            : base(mapper, repository)
        {
            userRoleRepository = repository;
        }

        private readonly IUserRoleRepository userRoleRepository;

        public async Task<PageInfoListDto<UserRoleDto, OrderUserRoleFields>> GetAllForUserAsync(UserRoleDtoFilter filter)
        {
            var orderSetting = mapper.Map<PagingOrderSetting<OrderUserRoleFields>>(filter.OrderSetting);
            var userRoleList =
                await userRoleRepository.GetByUserListAsync(filter.UserId, orderSetting, GetOptionsForDetails());

            int totalCount = await userRoleRepository.GetByUserCountAsync(filter.UserId);
            var retVal = GeneralTransferFromEntityFromDto<UserRole, UserRoleDto, OrderUserRoleFields>(userRoleList, totalCount, filter.OrderSetting);
            return retVal;
        }

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions
            {
                IsUser = true,
                IsRole = true,
            };
        }

        protected override async Task<string> CheckBeforeModificationAsync(UserRoleDto value, bool isNew = true)
        {
            var errors = string.Empty;
            try
            {
                var roleUser = await userRoleRepository.GetByRoleForUserAsync(value.UserId, value.RoleId);
                if (roleUser != null)
                {
                    return "Уже есть такая роль на данного пользователя";
                }
            }
            catch (Exception e)
            {
            }
            
            return errors;
        }

        protected override async Task<string> CkeckBeforeDeleteAsync(UserRole entity)
        {
            return string.Empty;
        }
    }
}
