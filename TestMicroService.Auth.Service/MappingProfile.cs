﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.InterfaceOfService;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto.User;

namespace TestMicroService.Auth.Service
{
    public class MappingProfile :  Profile
    {
        public MappingProfile()
        {
            UserMapping();
            RoleMapping();
            SesionMapping();
            GeneralMapping();
        }
        private void SesionMapping()
        {
            CreateMap<Session, SessionDto>();
            CreateMap<SessionDto, Session>()
                .ForMember(x => x.UserRole, p => p.Ignore())
                .ForMember(x => x.User, p => p.Ignore());
        }

        private void RoleMapping()
        {
            CreateMap<UserRole, UserRoleDto>();
            CreateMap<UserRoleDto, UserRole>();

            CreateMap<Role, RoleDto>();
        }

        private void UserMapping()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>()
                .ForMember(x => x.RoleList, p => p.Ignore())
                .ForMember(x => x.SessionList, p => p.Ignore());

            CreateMap<User, AuthenticateResponseDto>()
                .ForMember(x => x.Token, p => p.Ignore());

            CreateMap<RegistrationDto, UserDto>();

            CreateMap<RegistrationDto, User>()
                .ForMember(x => x.RoleList, p => p.Ignore())
                .ForMember(x => x.SessionList, p => p.Ignore());
        }

        private void GeneralMapping()
        {
            CreateMap(typeof(PagingOrderSettingDto<>), typeof(PagingOrderSetting<>));
        }
    }
}
