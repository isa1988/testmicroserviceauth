﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.InterfaceOfService;

namespace TestMicroService.Auth.Service
{
    public abstract class GeneralService<TEntity, TDto, TOrder> : IGeneralService<TDto, TOrder>
        where TEntity : class, IEntity
        where TDto : class, IServiceDto
        where TOrder : Enum
    {
        public GeneralService(IMapper mapper, IRepository<TEntity, TOrder> repository)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            repositoryBase = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        protected readonly IMapper mapper;
        protected readonly IRepository<TEntity, TOrder> repositoryBase;
        protected abstract Task<string> CheckBeforeModificationAsync(TDto value, bool isNew = true);

        public virtual async Task<EntityOperationResult<TDto>> CreateAsync(TDto createDto)
        {
            string errors = await CheckBeforeModificationAsync(createDto);
            if (!string.IsNullOrEmpty(errors))
            {
                return EntityOperationResult<TDto>.Failure().AddError(errors);
            }

            try
            {
                TEntity value = mapper.Map<TEntity>(createDto);
                var entity = await repositoryBase.AddAsync(value);
                await repositoryBase.SaveAsync();

                var dto = mapper.Map<TDto>(entity);

                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure().AddError(ex.Message);
            }
        }

        public abstract ResolveOptions GetOptionsForDetails();

        public virtual async Task<PageInfoListDto<TDto, TOrder>> GetAllAsync(PagingOrderSettingDto<TOrder> orderSetting)
        {
            var orderSet = mapper.Map<PagingOrderSetting<TOrder>>(orderSetting);
            var entityList = await repositoryBase.GetAllAsync(orderSet);
            var dtoList = mapper.Map<List<TDto>>(entityList);
            int totalCount = await repositoryBase.GetAllCountAsync();
            var retVal = GetPageInfoList(dtoList, totalCount, orderSetting);

            return retVal;
        }

        public virtual async Task<PageInfoListDto<TDto, TOrder>> GetAllDetailsAsync(PagingOrderSettingDto<TOrder> orderSetting)
        {
            var orderSet = mapper.Map<PagingOrderSetting<TOrder>>(orderSetting);
            var entityList = await repositoryBase.GetAllAsync(orderSet, GetOptionsForDetails());
            int totalCount = await repositoryBase.GetAllCountAsync();
            var retVal = GeneralTransferFromEntityFromDto<TEntity, TDto, TOrder>(entityList, totalCount, orderSetting);

            return retVal;
        }

        protected PageInfoListDto<TDtoC, TOrderField> GeneralTransferFromEntityFromDto<T, TDtoC, TOrderField>(List<T> entityList, int totalCount,
            PagingOrderSettingDto<TOrderField> orderSetting)
            where T : class
            where TDtoC : class, IServiceDto
            where TOrderField : Enum
        {
            var dtoList = mapper.Map<List<TDtoC>>(entityList);
            var retVal = GetPageInfoList(dtoList, totalCount, orderSetting);
            return retVal;
        }

        protected PageInfoListDto<T, TOrderField> GetPageInfoList<T, TOrderField>(List<T> value, int totalCount, PagingOrderSettingDto<TOrderField> orderSetting)
            where T : class, IServiceDto
            where TOrderField : Enum
        {
            var dto = new PageInfoListDto<T, TOrderField>
            {
                ValueList = value,
                TotalCount = totalCount,
                OrderSetting = orderSetting
            };
            return dto;
        }
    }

    public abstract class GeneralService<TEntity, TDto, TId, TOrder> : GeneralService<TEntity, TDto, TOrder>, IGeneralService<TDto, TId, TOrder>
        where TEntity : class, IEntity<TId>
        where TDto : class, IServiceDto<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        public GeneralService(IMapper mapper, IRepository<TEntity, TId, TOrder> repository) : base(mapper, repository)
        {
            repositoryBaseId = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        protected readonly IRepository<TEntity, TId, TOrder> repositoryBaseId;

        protected abstract Task<string> CkeckBeforeDeleteAsync(TEntity entity);

        public virtual async Task<EntityOperationResult<TDto>> GetByIdAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id);
                var dto = mapper.Map<TDto>(entity);
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure().AddError(ex.Message);
            }
        }

        public virtual async Task<EntityOperationResult<TDto>> GetByIdDetailsAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id, GetOptionsForDetails());
                var dto = mapper.Map<TDto>(entity);
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure().AddError(ex.Message);
            }
        }

        public virtual async Task<EntityOperationResult<TDto>> DeleteItemAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id);

                string error = await CkeckBeforeDeleteAsync(entity);
                if (!string.IsNullOrEmpty(error))
                {
                    return EntityOperationResult<TDto>.Failure().AddError(error);
                }

                var dto = mapper.Map<TDto>(entity);
                repositoryBaseId.Delete(entity);
                await repositoryBaseId.SaveAsync();
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure().AddError(ex.Message);
            }
        }
    }

    public abstract class GeneralServiceGuid<TEntity, TDto, TOrder> : GeneralService<TEntity, TDto, Guid, TOrder>, IGeneralService<TDto, Guid, TOrder>
        where TEntity : class, IEntity<Guid>
        where TDto : class, IServiceDto<Guid>
        where TOrder : Enum
    {
        public GeneralServiceGuid(IMapper mapper, IRepository<TEntity, Guid, TOrder> repository) : base(mapper, repository)
        {
        }
    }
}
