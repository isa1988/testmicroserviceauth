﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.InterfaceOfService;
using TestMicroService.Auth.InterfaceOfService.Contract;
using TestMicroService.Auth.InterfaceOfService.Filters;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto.User;
using TestMicroService.Auth.Primitive;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.Service
{
    public class UserService : GeneralServiceForDeleteGuid<User, UserDto, OrderUserFields>, IUserService
    {
        public UserService(IMapper mapper, 
                           IUserRepository repository, 
                           IUserRoleRepository userRoleRepository, 
                           ISessionRepository sessionRepository,
                           IRoleRepository roleRepository) 
            : base(mapper, repository)
        {
            this.userRepository = repository;
            this.userRoleRepository = userRoleRepository;
            this.sessionRepository = sessionRepository;
            this.roleRepository = roleRepository;
        }

        private readonly IUserRepository userRepository;
        private readonly IUserRoleRepository userRoleRepository;
        private readonly ISessionRepository sessionRepository;
        private readonly IRoleRepository roleRepository;

        public async Task<PageInfoListDto<UserDto, OrderUserFields>> GetAllFromSearchAsync(SearchByOneLineDtoFilter<OrderUserFields> filter)
        {
            var orderSet = mapper.Map<PagingOrderSetting<OrderUserFields>>(filter.OrderSetting);
            var entityList = await userRepository.GetAllFromSearchAsync(filter.Search, orderSet);
            var dtoList = mapper.Map<List<UserDto>>(entityList);
            int totalCount = await userRepository.GetAllFromSearchCountAsync(filter.Search);
            var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);

            return retVal;
        }
        public async Task<PageInfoListDto<UserDto, OrderUserFields>> GetAllFromSearchDetailAsync(SearchByOneLineDtoFilter<OrderUserFields> filter)
        {
            var orderSet = mapper.Map<PagingOrderSetting<OrderUserFields>>(filter.OrderSetting);
            var entityList = await userRepository.GetAllFromSearchAsync(filter.Search, orderSet, GetOptionsForDetails());
            var dtoList = mapper.Map<List<UserDto>>(entityList);
            int totalCount = await userRepository.GetAllFromSearchCountAsync(filter.Search);
            var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);

            return retVal;
        }

        public async Task<PageInfoListDto<RoleDto, OrderRoleFields>> GetRoleExcludeUserRoleAsync(RoleDtoFilter filter)
        {
            var orderSet = mapper.Map<PagingOrderSetting<OrderRoleFields>>(filter.OrderSetting);
            var roleList = await roleRepository.GetAllAsync(orderSet);
            var orderRoleUser = new PagingOrderSetting<OrderUserRoleFields>()
            {
                OrderField = OrderUserRoleFields.None,
                IsOrder = false,
                PageSize = orderSet.PageSize,
                StartPosition = orderSet.StartPosition,
                OrderDirection = OrderType.ASC
            };
            var userRoleList = await userRoleRepository.GetByUserListAsync(filter.UserId, orderRoleUser);

            var totalCount = await roleRepository.GetAllCountAsync();
            if (userRoleList.Count > 0)
            {
                var roleIdList = roleList.Select(x => x.Id).ToList();
                var userRoleIdList = userRoleList.Select(x => x.RoleId).ToList();
                var resultIdList = roleIdList.Except(userRoleIdList).ToList();
                roleList = roleList.Where(x => resultIdList.Any(m => x.Id == m)).ToList();

                var totalUserRoleCount = await userRoleRepository.GetByUserCountAsync(filter.UserId);
                totalCount -= totalUserRoleCount;
            }

            var dtoList = mapper.Map<List<RoleDto>>(roleList);
            
            var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);

            return retVal;
        }
        

        public async Task<EntityOperationResult<AuthenticateResponseDto>> LoginAsync(LoginDto model, string key, string issuer)
        {
            try
            {
                User user = await userRepository.GetUserAsync(model.Login, model.Password);
                if (user == null)
                {
                    throw new NullReferenceException("Неверный логин или пароль");
                }
                var userDto = new AuthenticateResponseDto();
                var roleList = await userRoleRepository.GetByUserListAsync(user.Id, new ResolveOptions{IsRole = true});
                if (roleList.Count == 0)
                {
                    throw new NullReferenceException($"There no roles for this user {user.Id}");
                }

                await sessionRepository.CloseAllOpenSesionAsync(user.Id);

                var session = new Session
                {
                    UserId = user.Id,
                    Token = GenerateToken(Guid.NewGuid().ToString(), key, issuer),
                };

                if (roleList.Count == 1)
                {
                    session.RoleId = roleList[0].Id;
                }
                //else
                //{
                    var roleIbnfoList = roleList.Select(x => x.Role).ToList();
                    userDto.Roles = mapper.Map<List<RoleDto>>(roleIbnfoList);

                //}

                session = await sessionRepository.AddAsync(session);
                await sessionRepository.SaveAsync();
                
                userDto.Token = session.Token;
                userDto.SessionId = session.Id;
                
                return EntityOperationResult<AuthenticateResponseDto>.Success(userDto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<AuthenticateResponseDto>.Failure().AddError(ex.Message);
            }
            
        }

        public async Task<EntityOperationResult<SessionDto>> RoleSelectAsync(LoginConfirmRoleDto model)
        {
            try
            {
                var session = await sessionRepository.GetActiveByIdAsync(model.SessionId);
                var role = await roleRepository.GetBySysNameAsync(model.RoleSys);
                var userRole = await userRoleRepository.GetByRoleForUserAsync(session.UserId, role.Id);
                session.RoleId = userRole.Id;
                sessionRepository.Update(session);
                await sessionRepository.SaveAsync();
                var sessionDto = mapper.Map<SessionDto>(session);

                return EntityOperationResult<SessionDto>.Success(sessionDto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<SessionDto>.Failure().AddError(ex.Message);
            }
        }

        public async Task<EntityOperationResult<UserDto>> RegistrationGeneralAsync(RegistrationDto registrationDto)
        {
            try
            {
                string errors = await GeneralCheckBeforeRegAsync(registrationDto);
                if (!string.IsNullOrWhiteSpace(errors))
                    return EntityOperationResult<UserDto>.Failure().AddError(errors);

                var user = mapper.Map<User>(registrationDto);
                user = await userRepository.AddAsync(user);
                await userRepository.SaveAsync();

                var roles = new List<RoleDto>() { new RoleDto { SysName = "User" } };
                var result = await AddRolesToUserAsync(user.Id, roles);
                
                if (!result.IsSuccess)
                {
                    return EntityOperationResult<UserDto>.Failure().AddError(result.GetErrorString());
                }

                var userDto = mapper.Map<UserDto>(user);

                return EntityOperationResult<UserDto>.Success(userDto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserDto>.Failure().AddError(ex.Message);
            }
        }

        private async Task<EntityOperationResult<UserRoleDto>> AddRolesToUserAsync(Guid userId,
            List<RoleDto> roleList)
        {
            try
            {
                UserRole retUserRole = null;
                Role role = null;
                foreach (var roleItem in roleList)
                {
                    role = await roleRepository.GetBySysNameAsync(roleItem.SysName);
                    retUserRole = await userRoleRepository.AddAsync(new UserRole
                    {
                        UserId = userId,
                        RoleId = role.Id
                    });
                }
                await userRoleRepository.SaveAsync();
                var roleDto = mapper.Map<UserRoleDto>(retUserRole);

                return EntityOperationResult<UserRoleDto>.Success(roleDto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserRoleDto>.Failure().AddError(ex.Message);
            }
        }
        private async Task<string> GeneralCheckBeforeRegAsync(RegistrationDto registrationDto)
        {
            var userDto = mapper.Map<UserDto>(registrationDto);
            string errors = await CheckBeforeModificationAsync(userDto);
            if (string.IsNullOrWhiteSpace(registrationDto.Password))
                errors += "Не заполнен пароль" + Environment.NewLine;
            if (registrationDto.Password != registrationDto.PasswordConfirm)
                errors += "Пароль не совпадает с подверждением" + Environment.NewLine;
            return errors;
        }
        
        private string GenerateToken(string tokenRequest, string key, string issuer)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(issuer,
                issuer,
                new Claim[] { new Claim(ClaimTypes.Name, tokenRequest), },
                //expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<Guid> GetUserIdBySessionAsync(Guid sessionId)
        {
            var user = await userRepository.GetUserbySesion(sessionId);
            return user != null ? user.Id : default(Guid);
        }
        public async Task<EntityOperationResult<UserDto>> EditAsync(UserEditDto editDto)
        {
            var user = await userRepository.GetByIdAsync(editDto.Id);
            var result = await EditCurrentAsync(user, editDto);
            return result;
        }

        public async Task<EntityOperationResult<UserDto>> EditCurrentAsync(UserEditDto editDto)
        {
            var user = await userRepository.GetUserbySesion(editDto.Id);
            var result = await EditCurrentAsync(user, editDto);
            return result;
        }

        private async Task<EntityOperationResult<UserDto>> EditCurrentAsync(User user, UserEditDto editDto)
        {
            try
            {
                if (user == null)
                {
                    return EntityOperationResult<UserDto>.Failure().AddError("Не найден пользователь");
                }

                user.Name = editDto.Name;

                userRepository.Update(user);

                await userRepository.SaveAsync();
                var userDto = mapper.Map<UserDto>(user);

                return EntityOperationResult<UserDto>.Success(userDto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserDto>.Failure().AddError(ex.Message);
            }
        }

        public async Task<EntityOperationResult<UserDto>> ChangePasswordAsync(UserChangePasswordDto changePasswordDto)
        {
            var user = await userRepository.GetByIdAsync(changePasswordDto.Id);
            var result = await ChangePasswordAsync(user, changePasswordDto);
            return result;
        }

        public async Task<EntityOperationResult<UserDto>> ChangePasswordCurrentAsync(
            UserChangePasswordDto changePasswordDto)
        {
            var user = await userRepository.GetUserbySesion(changePasswordDto.Id);
            var result = await ChangePasswordAsync(user, changePasswordDto);
            return result;
        }

        private async Task<EntityOperationResult<UserDto>> ChangePasswordAsync(User user, UserChangePasswordDto changePasswordDto)
        {
            try
            {
                if (user == null)
                {
                    return EntityOperationResult<UserDto>.Failure().AddError("Не найден пользователь");
                }

                if (changePasswordDto.IsCheck)
                {

                    var passwordCheck = userRepository.GetEncryptedPassword(changePasswordDto.PasswordCurrent);
                    if (passwordCheck != user.Password)
                    {
                        return EntityOperationResult<UserDto>.Failure().AddError("Текущий пароль неверен");
                    }
                }

                if (changePasswordDto.PasswordNew != changePasswordDto.PasswordNewConfirm)
                {
                    return EntityOperationResult<UserDto>.Failure().AddError("Новый пароль не совпадает с подвержденным");
                }

                user.Password = userRepository.GetEncryptedPassword(changePasswordDto.PasswordNew);

                userRepository.Update(user);

                await userRepository.SaveAsync();
                var userDto = mapper.Map<UserDto>(user);

                return EntityOperationResult<UserDto>.Success(userDto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserDto>.Failure().AddError(ex.Message);
            }
        }

        public async Task<EntityOperationResult<UserCaptionDto>> GetUserCaptionAsync(Guid sessionId)
        {
            try
            {
                var user = await userRepository.GetUserbySesion(sessionId);
                if (user == null)
                {
                    return EntityOperationResult<UserCaptionDto>.Failure().AddError("Не найден пользователь");
                }

                var userCaption = new UserCaptionDto
                {
                    Title = user.Name
                };

                
                return EntityOperationResult<UserCaptionDto>.Success(userCaption);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserCaptionDto>.Failure().AddError(ex.Message);
            }
        }

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions();
        }


        protected override async Task<string> CheckBeforeModificationAsync(UserDto value, bool isNew = true)
        {
            string errors = string.Empty;
            if (string.IsNullOrWhiteSpace(value.Name))
                errors += "Не заполнено поле Имя" + Environment.NewLine;
            if (string.IsNullOrWhiteSpace(value.Login))
                errors += "Не заполнено поле Логин имя" + Environment.NewLine;
            if (string.IsNullOrWhiteSpace(value.Email))
                errors += "Не заполнено поле Электронный ящик" + Environment.NewLine;
            if (string.IsNullOrWhiteSpace(value.Password))
                errors += "Не заполнено поле Пароль" + Environment.NewLine;
            if (string.IsNullOrWhiteSpace(value.Name))
                errors += "Не заполнено поле Имя" + Environment.NewLine;
            if (isNew)
            {
                if (await userRepository.IsEqualEmailAsync(value.Email, value.Login))
                    errors += "Такой email уже используется" + Environment.NewLine;
            }
            else
            {
                if (await userRepository.IsEqualEmailAsync(value.Email, value.Login, value.Id))
                    errors += "Такой email уже используется" + Environment.NewLine;
            }
            return errors;
        }

        protected override async Task<string> CkeckBeforeDeleteAsync(User entity)
        {
            return string.Empty;
        }

    }
}
