﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.InterfaceOfService;
using TestMicroService.Auth.InterfaceOfService.Contract;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.Service
{
    public class SessionService : GeneralServiceGuid<Session, SessionDto, OrderSessionFields>, ISessionService
    {
        public SessionService(IMapper mapper, ISessionRepository repository)
            : base(mapper, repository)
        {
            this.sessionRepository = repository;
        }

        private readonly ISessionRepository sessionRepository;

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions
            {
                IsRole = true,
                IsUser = true
            };
        }

        public async Task<EntityOperationResult<SessionDto>> GetActiveByIdAsync(Guid id)
        {
            try
            {
                var session = await sessionRepository.GetActiveByIdAsync(id);
                var sessionDto = mapper.Map<SessionDto>(session);
                return EntityOperationResult<SessionDto>.Success(sessionDto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<SessionDto>.Failure().AddError(ex.Message);
            }
        }

        public async Task<EntityOperationResult<SessionDto>> GetActiveByIdDetailsAsync(Guid id)
        {
            try
            {
                var session = await sessionRepository.GetActiveByIdAsync(id, GetOptionsForDetails());
                var sessionDto = mapper.Map<SessionDto>(session);
                return EntityOperationResult<SessionDto>.Success(sessionDto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<SessionDto>.Failure().AddError(ex.Message);
            }
        }
        public async Task<EntityOperationResult<SessionDto>> GetActiveByTokenAsync(string token)
        {
            try
            {
                var session = await sessionRepository.GetActiveByTokenAsync(token);
                var sessionDto = mapper.Map<SessionDto>(session);
                return EntityOperationResult<SessionDto>.Success(sessionDto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<SessionDto>.Failure().AddError(ex.Message);
            }
        }

        public async Task<List<SessionDto>> GetActiveByUserListAsync(List<Guid> userListId)
        {
            var sessionList = await sessionRepository.GetActiveByUserListAsync(userListId);
            var sessionDtoList = mapper.Map<List<SessionDto>>(sessionList);
            return sessionDtoList;
        }

        public async Task<EntityOperationResult<SessionDto>> GetActiveByTokenDetailAsync(string token)
        {
            try
            {
                var session = await sessionRepository.GetActiveByTokenAsync(token, GetOptionsForDetails());
                var sessionDto = mapper.Map<SessionDto>(session);
                return EntityOperationResult<SessionDto>.Success(sessionDto);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<SessionDto>.Failure().AddError(ex.Message);
            }
        }

        protected override async Task<string> CheckBeforeModificationAsync(SessionDto value, bool isNew = true)
        {
            string error = string.Empty;
            return error;
        }

        protected override async Task<string> CkeckBeforeDeleteAsync(Session entity)
        {
            return string.Empty;
        }

    }
}
