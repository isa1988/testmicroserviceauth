﻿using System;

namespace TestMicroService.Auth.Primitive
{
    public enum Deleted
    {
        UnDeleted,
        Deleted
    }
}
