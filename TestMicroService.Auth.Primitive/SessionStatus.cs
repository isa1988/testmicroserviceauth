﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMicroService.Auth.Primitive
{
    public enum SessionStatus 
    {
        Active = 0,
        Closed = 1
    }
}
