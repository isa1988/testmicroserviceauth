﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMicroService.Auth.Primitive
{
    public enum SettingSelectForDelete 
    {
        All = 0,
        OnlyDelete,
        OnlyUnDelete,
    }
}
