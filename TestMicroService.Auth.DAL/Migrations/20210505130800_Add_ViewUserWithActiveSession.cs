﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestMicroService.Auth.DAL.Migrations
{
    public partial class Add_ViewUserWithActiveSession : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"Create VIEW UserWithActiveSessionView AS SELECT
                                u.""Id"" as ""UserId"", 
                                u.""Name"" as ""UserName"", 
                                u.""Email"",
                                u.""Password"",
                                u.""Login"",
                                u.""IsDeleted"",
                                s.""Id"" as ""SessionId"",
                                s.""Token"",
                                s.""Status"",
                                s.""CloseDate"",
                                r.""Name"" as ""RoleName"",
                                r.""SysName""
                                FROM ""User"" as u LEFT JOIN
                                ""Session"" as s ON u.""Id"" = s.""UserId"" LEFT JOIN
                                ""UserRole"" as ur ON s.""RoleId"" = ur.""Id"" LEFT JOIN
                                ""Role"" as r ON ur.""RoleId"" = r.""Id""");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP VIEW UserWithActiveSessionView");
        }
    }
}
