﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestMicroService.Auth.Core.Entity;

namespace TestMicroService.Auth.DAL.Data.Configuration
{
    class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(200);
            builder.Property(e => e.SysName).IsRequired().HasMaxLength(100);
            builder.HasIndex(e => e.SysName).IsUnique();
        }
    }
}
