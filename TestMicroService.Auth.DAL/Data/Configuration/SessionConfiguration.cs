﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestMicroService.Auth.Core.Entity;

namespace TestMicroService.Auth.DAL.Data.Configuration
{
    class SessionConfiguration : IEntityTypeConfiguration<Session>
    {
        public void Configure(EntityTypeBuilder<Session> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.CloseDate);
            builder.Property(e => e.Token).IsRequired().HasMaxLength(4000);
            builder.Property(e => e.Info).HasMaxLength(4000);
            builder.HasOne(e => e.User)
                .WithMany(p => p.SessionList)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
