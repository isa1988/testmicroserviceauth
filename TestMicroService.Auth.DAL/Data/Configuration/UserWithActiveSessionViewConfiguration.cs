﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestMicroService.Auth.Core.View;

namespace TestMicroService.Auth.DAL.Data.Configuration
{
    public class UserWithActiveSessionViewConfiguration : IEntityTypeConfiguration<UserWithActiveSessionView>
    {
        public void Configure(EntityTypeBuilder<UserWithActiveSessionView> builder)
        {
            builder.HasNoKey();
            builder.ToView("userwithactivesessionview");
        }
    }
}
