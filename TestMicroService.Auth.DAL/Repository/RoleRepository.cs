﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.Primitive;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.DAL.Repository
{
    public class RoleRepository : RepositoryForDeletedGuid<Role, OrderRoleFields>, IRoleRepository
    {
        public RoleRepository(ProjectDB contextDB) : base(contextDB)
        {
        }

        public async Task<Role> GetBySysNameAsync(string sysName)
        {
            var role = await ResolveInclude(null).FirstOrDefaultAsync(x => x.SysName == sysName && x.IsDeleted == Deleted.UnDeleted);
            if (role == null)
                throw new NullReferenceException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(role);
            return role;
        }

        public async Task<bool> IsEqualsSysNameAsync(string sysName)
        {
            var isEquals = await ResolveInclude(null).AnyAsync(x => x.SysName == sysName && x.IsDeleted == Deleted.UnDeleted);
            return isEquals;
        }

        protected override void ClearDbSetForInclude(Role entity)
        {

        }

        protected override IQueryable<Role> OrderSort(IQueryable<Role> query, PagingOrderSetting<OrderRoleFields> pagingOrderSetting = null)
        {
            if (pagingOrderSetting == null || !pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderRoleFields.None)
                return query;
            return query;
        }

        protected override IQueryable<Role> ResolveInclude(ResolveOptions resolveOptions)
        {
            IQueryable<Role> query = dbSetQueryable;

            return query;
        }
    }
}
