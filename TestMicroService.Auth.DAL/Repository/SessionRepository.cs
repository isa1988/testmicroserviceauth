﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.Primitive;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.DAL.Repository
{
    public class SessionRepository : RepositoryGuid<Session, OrderSessionFields>, ISessionRepository
    {
        public SessionRepository(ProjectDB contextDB)
            : base(contextDB)
        {
        }

        public async Task<Session> GetActiveByIdAsync(Guid id, ResolveOptions resolveOptions = null)
        {
            var session = await ResolveInclude(resolveOptions).FirstOrDefaultAsync(x => x.Id == id && x.Status == SessionStatus.Active);
            if (session == null)
                throw new NullReferenceException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(session);
            return session;
        }

        public async Task<Session> GetActiveByTokenAsync(string token, ResolveOptions resolveOptions = null)
        {
            var session = await ResolveInclude(resolveOptions).FirstOrDefaultAsync(x => x.Token.Trim() == token.Trim() && x.Status == SessionStatus.Active);
            if (session == null)
                throw new NullReferenceException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(session);
            return session;
        }

        public async Task CloseAllOpenSesionAsync(Guid userId)
        {
            var sessionList = await ResolveInclude(null).Where(x => x.UserId == userId && x.Status == SessionStatus.Active).ToListAsync();
            if (sessionList?.Count > 0)
            {
                foreach (var session in sessionList)
                {
                    session.Status = SessionStatus.Closed;
                    session.CloseDate = DateTime.UtcNow;
                    Update(session);
                }
            }
        }

        public async Task<List<Session>> GetActiveByUserListAsync(List<Guid> userListId, ResolveOptions resolveOptions = null)
        {
            var sessionList = await ResolveInclude(resolveOptions)
                .Where(f => userListId.Any(g => g == f.UserId) && f.Status == SessionStatus.Active)
                .Select(d => d)
                .ToListAsync();

            return sessionList;
        }
        protected override void ClearDbSetForInclude(Session entity)
        {

        }

        protected override IQueryable<Session> OrderSort(IQueryable<Session> query, PagingOrderSetting<OrderSessionFields> pagingOrderSetting = null)
        {
            if (pagingOrderSetting == null || !pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderSessionFields.None)
                return query;

            return query;
        }

        protected override IQueryable<Session> ResolveInclude(ResolveOptions resolveOptions)
        {
            IQueryable<Session> query = dbSetQueryable;
            if (resolveOptions == null)
            {
                return query;
            }

            if (resolveOptions.IsUser)
            {
                query = query.Include(x => x.User);
            }

            if (resolveOptions.IsRole)
            {
                query = query.Include(x => x.UserRole);
            }

            return query;
        }

    }
}
