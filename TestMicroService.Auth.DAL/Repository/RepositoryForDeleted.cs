﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.DAL.Repository
{
    public abstract class RepositoryForDeleted<T, TOrder> : Repository<T, TOrder>, IRepositoryForDeleted<T, TOrder>
        where T : class, IEntityForDelete
        where TOrder : Enum
    {
        public RepositoryForDeleted(ProjectDB contextDB)
            : base(contextDB)
        {
        }

        public override async Task<List<T>> GetAllAsync(PagingOrderSetting<TOrder> order = null, ResolveOptions resolveOptions = null)
        {
            SetDBSetQueryable(SettingSelectForDelete.OnlyUnDelete);
            var entities = await base.GetAllAsync(order, resolveOptions);
            return entities;
        }

        public async Task<List<T>> GetAllAsync(SettingSelectForDelete settingSelectForDelete, PagingOrderSetting<TOrder> order = null, ResolveOptions resolveOptions = null)
        {
            SetDBSetQueryable(settingSelectForDelete);
            var entities = await base.GetAllAsync(order, resolveOptions);
            return entities;
        }

        public override EntityEntry<T> Delete(T entity)
        {
            entity.IsDeleted = Deleted.Deleted;
            EntityEntry<T> entryResult = dbSet.Update(entity);

            return entryResult;
        }

        public override async Task<EntityEntry<T>> DeleteAsync(T entity)
        {
            Task<EntityEntry<T>> task = new Task<EntityEntry<T>>(() =>
            {
                entity.IsDeleted = Deleted.Deleted;
                return dbSet.Update(entity);
            });
            var entry = await task;
            return entry;
        }

        public async Task<EntityEntry<T>> DeleteFromDbAsync(T entity)
        {
            Task<EntityEntry<T>> task = new Task<EntityEntry<T>>(() =>
            {
                return dbSet.Remove(entity);
            });
            var entry = await task;
            return entry;
        }

        protected void SetDBSetQueryable(SettingSelectForDelete settingSelectForDelete)
        {
            if (settingSelectForDelete == SettingSelectForDelete.OnlyDelete)
            {
                dbSetQueryable = dbSet.Where(x => x.IsDeleted == Deleted.Deleted);
            }
            else if (settingSelectForDelete == SettingSelectForDelete.OnlyUnDelete)
            {
                dbSetQueryable = dbSet.Where(x => x.IsDeleted == Deleted.UnDeleted);
            }
        }
    }

    public abstract class RepositoryForDeleted<T, TId, TOrder> : RepositoryForDeleted<T, TOrder>, IRepositoryForDeleted<T, TId, TOrder>
        where T : class, IEntityForDelete<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        public RepositoryForDeleted(ProjectDB contextDB, bool isIdAutoIncrement = false)
            : base(contextDB)
        {
            this.isIdAutoIncrement = isIdAutoIncrement;
        }

        private readonly bool isIdAutoIncrement;

        public virtual async Task<T> GetByIdAsync(TId id, ResolveOptions resolveOptions = null)
        {
            var entity = await GetByIdAsync(id, SettingSelectForDelete.OnlyUnDelete, resolveOptions);
            ClearDbSetForInclude(entity);
            return entity;
        }

        public virtual async Task<T> GetByIdAsync(TId id, SettingSelectForDelete settingSelectForDelete, ResolveOptions resolveOptions = null)
        {
            SetDBSetQueryable(settingSelectForDelete);
            var entity = await ResolveInclude(resolveOptions).FirstOrDefaultAsync(x => x.Id.Equals(id));
            if (entity == null)
                throw new NullReferenceException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(entity);
            return entity;
        }

        protected abstract TId GetNewId();

        public override async Task<T> AddAsync(T entity)
        {
            if (!isIdAutoIncrement)
            {
                entity.Id = GetId(GetNewId());
            }

            return await base.AddAsync(entity);
        }

        private TId GetId(TId value)
        {
            while (dbSet.Any(p => p.Id.Equals(value)))
            {

                value = GetNewId();
                return GetId(value);
            }

            return value;
        }
    }

    public abstract class RepositoryForDeletedGuid<T, TOrder> : RepositoryForDeleted<T, Guid, TOrder>, IRepositoryForDeleted<T, Guid, TOrder>
        where T : class, IEntityForDelete<Guid>
        where TOrder : Enum
    {
        public RepositoryForDeletedGuid(ProjectDB contextDB)
            : base(contextDB)
        {
        }

        protected override Guid GetNewId()
        {
            var retVal = Guid.NewGuid();
            return retVal;
        }
    }
}
