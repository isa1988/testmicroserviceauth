﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;
using TestMicroService.Auth.Primitive;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.DAL.Repository
{
    public class UserRepository : RepositoryForDeletedGuid<User, OrderUserFields>, IUserRepository
    {
        public UserRepository(ProjectDB contextDB) : base(contextDB)
        {
        }

        public async Task<List<User>> GetAllFromSearchAsync(string search, PagingOrderSetting<OrderUserFields> pagingOrderSetting, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions);

            query = query.Where(x => x.IsDeleted == Deleted.UnDeleted);
            query = query.Where(x => x.Name.Contains(search) || x.Login.Contains(search));
            var entities = await GetListAsync(query, pagingOrderSetting);

            return entities;
        }

        public async Task<int> GetAllFromSearchCountAsync(string search)
        {
            var query = ResolveInclude(null);

            query = query.Where(x => x.IsDeleted == Deleted.UnDeleted);
            query = query.Where(x => x.Name.Contains(search) || x.Login.Contains(search));


            var count = await query.CountAsync();
            return count;
        }

        public async Task<bool> IsEqualEmailAsync(string email, string login, Guid excludingId = default(Guid))
        {
            var isEqual = false;
            if (excludingId == Guid.Empty)
            {
                isEqual = await dbSetQueryable.AnyAsync(x => x.Email == email || x.Login == login);
            }
            else
            {
                isEqual = await dbSetQueryable.AnyAsync(x => (x.Email == email || x.Login == login) && x.Id != excludingId);
            }

            return isEqual;
        }

        public async Task<User> GetUserAsync(string login, string password)
        {
            password = HashPassword(password);
            var entity = await ResolveInclude(null).FirstOrDefaultAsync(x => x.IsDeleted == Deleted.UnDeleted &&
                                                                                         x.Login == login && x.Password == password);

            return entity;
        }

        public async Task<User> GetUserbySesion(Guid sessionId)
        {
            var session = await contextDB.Session.FirstOrDefaultAsync(x => x.Id == sessionId);
            var entity = await ResolveInclude(null).FirstOrDefaultAsync(x => x.IsDeleted == Deleted.UnDeleted && x.Id == session.UserId);
            return entity;
        }

        public override Task<User> AddAsync(User entity)
        {
            entity.Password = HashPassword(entity.Password);
            return base.AddAsync(entity);
        }

        string HashPassword(string password)
        {
            var sha1 = new MD5CryptoServiceProvider();
            var data = Encoding.ASCII.GetBytes(password);
            var sha1data = sha1.ComputeHash(data);

            return Convert.ToBase64String(sha1data);
        }

        public string GetEncryptedPassword(string password)
        {
            var retVal = HashPassword(password);
            return retVal;
        }

        protected override void ClearDbSetForInclude(User entity)
        {
            entity.SessionList?.ForEach(f => { f.User = null; });
            entity.RoleList?.ForEach(f => { f.User = null; });
        }

        protected override IQueryable<User> OrderSort(IQueryable<User> query, PagingOrderSetting<OrderUserFields> pagingOrderSetting = null)
        {
            if (pagingOrderSetting == null || !pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderUserFields.None)
                return query;

            if (pagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderUserFields.Name:
                        {
                            query = query.OrderBy(x => x.Name);
                            break;
                        }
                    case OrderUserFields.Email:
                        {
                            query = query.OrderBy(x => x.Login);
                            break;
                        }
                    case OrderUserFields.Login:
                        {
                            query = query.OrderBy(x => x.Login);
                            break;
                        }
                }
            }
            else
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderUserFields.Name:
                        {
                            query = query.OrderByDescending(x => x.Name);
                            break;
                        }
                    case OrderUserFields.Email:
                    {
                        query = query.OrderByDescending(x => x.Login);
                        break;
                        }
                    case OrderUserFields.Login:
                    {
                        query = query.OrderByDescending(x => x.Login);
                        break;
                    }
                }
            }

            return query;
        }

        protected override IQueryable<User> ResolveInclude(ResolveOptions resolveOptions)
        {
            IQueryable<User> query = dbSetQueryable;
            if (resolveOptions == null)
            {
                return query;
            }

            if (resolveOptions.IsSessions)
            {
                query = query.Include(x => x.SessionList);
            }

            if (resolveOptions.IsRoleList)
            {
                query = query.Include(x => x.RoleList);
            }

            return query;
        }
    }
}
