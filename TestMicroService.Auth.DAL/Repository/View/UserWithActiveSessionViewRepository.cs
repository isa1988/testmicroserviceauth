﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestMicroService.Auth.Core.Contract.View;
using TestMicroService.Auth.Core.View;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.DAL.Repository.View
{
    public class UserWithActiveSessionViewRepository : IUserWithActiveSessionViewRepository
    {
        public UserWithActiveSessionViewRepository(ProjectDB contextDB)
        {
            userWithActiveSessionView = contextDB.Set<UserWithActiveSessionView>();
        }

        private DbSet<UserWithActiveSessionView> userWithActiveSessionView { get; set; }

        public async Task<List<UserWithActiveSessionView>> GetAllUsersWithActive()
        {
            var entityList = await userWithActiveSessionView.Where(x => x.IsDeleted != Deleted.Deleted &&
                                                                        x.Status != SessionStatus.Closed).ToListAsync();
            return entityList;
        }
    }
}
