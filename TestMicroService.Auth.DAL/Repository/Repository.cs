﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.Helper;

namespace TestMicroService.Auth.DAL.Repository
{
    public abstract class RepositoryBase<T, TOrder>
        where T : class
        where TOrder : Enum
    {
        protected virtual void ClearDbSetForInclude(List<T> entities)
        {
            foreach (var entity in entities)
            {
                ClearDbSetForInclude(entity);
            }
        }

        protected async Task<List<T>> GetListAsync(IQueryable<T> query, PagingOrderSetting<TOrder> pagingOrderSetting)
        {
            if (pagingOrderSetting != null)
                query = query.Skip(pagingOrderSetting.StartPosition)
                    .Take(pagingOrderSetting.PageSize);

            List<T> entities = await OrderSort(query, pagingOrderSetting).ToListAsync();
            ClearDbSetForInclude(entities);
            return entities;
        }

        protected abstract void ClearDbSetForInclude(T entity);

        protected abstract IQueryable<T> ResolveInclude(ResolveOptions resolveOptions);
        protected abstract IQueryable<T> OrderSort(IQueryable<T> query, PagingOrderSetting<TOrder> pagingOrderSetting = null);
    }


    public abstract class Repository<T, TOrder> : RepositoryBase<T, TOrder>, IRepository<T, TOrder>
        where T : class, IEntity
        where TOrder : Enum
    {
        public Repository(ProjectDB contextDB)
        {
            this.contextDB = contextDB;
            this.dbSet = this.contextDB.Set<T>();
            this.dbSetQueryable = dbSet;
        }

        protected ProjectDB contextDB;
        protected DbSet<T> dbSet { get; set; }
        protected IQueryable<T> dbSetQueryable { get; set; }
        public virtual async Task<T> AddAsync(T entity)
        {
            var entry = await dbSet.AddAsync(entity);

            return entry.Entity;
        }
        public virtual async Task<List<T>> GetAllAsync(PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions);

            var entities = await GetListAsync(query, pagingOrderSetting);

            return entities;
        }

        public virtual async Task<int> GetAllCountAsync()
        {
            var count = await ResolveInclude(null).CountAsync();
            return count;
        }

        public virtual async Task<EntityEntry<T>> UpdateAsync(T entity)
        {
            Task<EntityEntry<T>> task = new Task<EntityEntry<T>>(() =>
            {
                return dbSet.Update(entity);
            });
            var entry = await task;
            return entry;
        }

        public virtual async Task<EntityEntry<T>> DeleteAsync(T entity)
        {
            Task<EntityEntry<T>> task = new Task<EntityEntry<T>>(() =>
            {
                return dbSet.Remove(entity);
            });
            var entry = await task;
            return entry;
        }

        public virtual EntityEntry<T> Update(T entity)
        {
            return dbSet.Update(entity);
        }

        public virtual EntityEntry<T> Delete(T entity)
        {
            return dbSet.Remove(entity);
        }

        public virtual async Task SaveAsync()
        {
            await contextDB.SaveChangesAsync();
        }
    }

    public abstract class Repository<T, TId, TOrder> : Repository<T, TOrder>, IRepository<T, TId, TOrder>
        where T : class, IEntity<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        public Repository(ProjectDB context, bool isIdAutoIncrement = false)
            : base(context)
        {
            this.isIdAutoIncrement = isIdAutoIncrement;
        }

        private readonly bool isIdAutoIncrement;
        public virtual async Task<T> GetByIdAsync(TId id, ResolveOptions resolveOptions = null)
        {
            var entity = await ResolveInclude(resolveOptions).FirstOrDefaultAsync(x => x.Id.Equals(id));
            if (entity == null)
                throw new NullReferenceException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(entity);
            return entity;
        }

        protected abstract TId GetNewId();

        public override async Task<T> AddAsync(T entity)
        {
            if (!isIdAutoIncrement)
            {
                entity.Id = GetId(GetNewId());
            }

            return await base.AddAsync(entity);
        }

        private TId GetId(TId value)
        {
            while (dbSet.Any(p => p.Id.Equals(value)))
            {

                value = GetNewId();
                return GetId(value);
            }

            return value;
        }
    }

    public abstract class RepositoryGuid<T, TOrder> : Repository<T, Guid, TOrder>, IRepository<T, Guid, TOrder>
        where T : class, IEntity<Guid>
        where TOrder : Enum
    {
        public RepositoryGuid(ProjectDB contextDB)
            : base(contextDB)
        {
        }

        protected override Guid GetNewId()
        {
            var retVal = Guid.NewGuid();
            return retVal;
        }
    }
}
