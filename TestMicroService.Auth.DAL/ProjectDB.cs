﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.Core.View;
using TestMicroService.Auth.DAL.Data.Configuration;

namespace TestMicroService.Auth.DAL
{
    public class ProjectDB : DbContext
    {
        public ProjectDB(DbContextOptions<ProjectDB> options)
            : base(options)
        {
        }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<Session> Session { get; set; }
        public virtual DbSet<UserWithActiveSessionView> UserWithActiveSessionView { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfiguration());
            modelBuilder.ApplyConfiguration(new SessionConfiguration());
            modelBuilder.ApplyConfiguration(new UserWithActiveSessionViewConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
