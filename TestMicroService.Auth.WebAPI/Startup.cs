using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Hangfire;
using Hangfire.Common;
using Hangfire.PostgreSql;
using Microsoft.OpenApi.Models;
using TestMicroService.Auth.Core;
using TestMicroService.Auth.Core.Contract;
using TestMicroService.Auth.Core.Contract.View;
using TestMicroService.Auth.Core.Entity;
using TestMicroService.Auth.DAL;
using TestMicroService.Auth.DAL.Data;
using TestMicroService.Auth.DAL.Repository;
using TestMicroService.Auth.DAL.Repository.View;
using TestMicroService.Auth.InterfaceOfService.Contract;
using TestMicroService.Auth.Service;
using TestMicroService.Auth.WebAPI.AppStart;
using TestMicroService.Auth.WebAPI.Helper;
using TestMicroService.Auth.WebAPI.IdentitySettings;
using TestMicroService.Auth.WebAPI.UserMiddleware;

namespace TestMicroService.Auth.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabaseContext(Configuration);
            services.AddAutoMapperCustom();

            services.Configure<JwtSetting>(Configuration.GetSection("Jwt"));

            services.AddHangfire(options =>
            {
                options.UsePostgreSqlStorage(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddSwaggerGen(swagger =>
            {
                //This is to generate the Default UI of Swagger Documentation  
                swagger.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "JWT Token Authentication API",
                    Description = "ASP.NET Core 3.1 Web API"
                });
                // To Enable authorization using Swagger (JWT)  
                swagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "AuthorizationToken",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                });
                swagger.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] {}

                    }
                });
                // Set the comments path for the Swagger JSON and UI.
                //var xmlFile = "WebAuth.xml";
                //var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //swagger.IncludeXmlComments(xmlPath);
            });
            
            services.AddSingleton<IAppConfiguration, AppConfiguration>();

            /*services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                /*.AddOperationalStore(options =>
                {
                    options.EnableTokenCleanup = true;
                    options.TokenCleanupInterval = 30; // interval in seconds
                })
                .AddInMemoryApiResources(AppConfiguration.GetApiResources())
                .AddInMemoryClients(AppConfiguration.GetClients())
                .AddTestUsers(AppConfiguration.Users)
                .AddProfileService<ProfileService>();*/

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, 
            IWebHostEnvironment env,
            IDataBaseInitializer baseInitializer,
            IAppConfiguration appConfiguration,
            IUserWithActiveSessionViewRepository userWithActiveSessionViewRepository,
            IRecurringJobManager recurringJobs)
        {
            app.UseAuthenticationSchemeMiddleware();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            //app.UseIdentityServer();

            app.UseRouting();

            app.UseAuthorization();
            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API v1");
            });

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
            });

            app.UseHangfireServer();

            baseInitializer.InitializeForDeveloperAsync().GetAwaiter().GetResult();
            appConfiguration.SetUserWithActiveSession(userWithActiveSessionViewRepository).GetAwaiter().GetResult();

            string timePeriod = Configuration.GetSection("UpdateUserList:TimePeriod").Value;
            recurringJobs.AddOrUpdate("UpdateUser", Job.FromExpression<IUpdateUserList>(x =>
                x.Done()), timePeriod, TimeZoneInfo.Local);
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterType<RoleRepository>().As<IRoleRepository>();
            builder.RegisterType<UserRoleRepository>().As<IUserRoleRepository>();
            builder.RegisterType<SessionRepository>().As<ISessionRepository>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<UserWithActiveSessionViewRepository>().As<IUserWithActiveSessionViewRepository>();

            builder.RegisterType<DataBaseInitializer>().As<IDataBaseInitializer>();

            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<RoleService>().As<IRoleService>();
            builder.RegisterType<UserRoleService>().As<IUserRoleService>();
            builder.RegisterType<SessionService>().As<ISessionService>();
            builder.RegisterType<UpdateUserList>().As<IUpdateUserList>();
        }
    }
}
