﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMicroService.Auth.Core.Contract.View;
using TestMicroService.Auth.Core.View;

namespace TestMicroService.Auth.WebAPI.IdentitySettings
{
    public interface IAppConfiguration
    {
        //IEnumerable<ApiResource> GetApiResources();
        //IEnumerable<Client> GetClients();
        Task SetUserWithActiveSession(IUserWithActiveSessionViewRepository userWithActiveSessionViewRepository);
        Task<(string, string)> GetNameAndRole(string idSession);
        Task<bool> IsActive(string idSession);
        Task SetActiveUserAsync(Guid sessionId, string token, string login, string role);
        Task<string> GetLoginBySessionAsync(Guid sessionId);
        Task<(string, string, string)> GetInoSession(string token);
        //List<TestUser> Users { get; set; }
        //List<UserWithActiveSessionView> UserWithActiveSession { get; set; }
    }
}
