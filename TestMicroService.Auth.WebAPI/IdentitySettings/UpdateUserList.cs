﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMicroService.Auth.Core.Contract.View;

namespace TestMicroService.Auth.WebAPI.IdentitySettings
{
    public class UpdateUserList : IUpdateUserList
    {
        public UpdateUserList(IUserWithActiveSessionViewRepository userWithActiveSessionViewRepository,
            IAppConfiguration appConfiguration)
        {
            this.userWithActiveSessionViewRepository = userWithActiveSessionViewRepository;
            this.appConfiguration = appConfiguration;
        }

        private readonly IUserWithActiveSessionViewRepository userWithActiveSessionViewRepository;
        private readonly IAppConfiguration appConfiguration;
        public async Task Done()
        {
            await appConfiguration.SetUserWithActiveSession(userWithActiveSessionViewRepository);
        }
    }
}
