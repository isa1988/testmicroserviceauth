﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TestMicroService.Auth.Core.Contract.View;
using TestMicroService.Auth.Core.View;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.WebAPI.IdentitySettings
{
    public class AppConfiguration : IAppConfiguration
    {
        private List<UserWithActiveSessionView> UserWithActiveSession { get; set; } = new List<UserWithActiveSessionView>();
        public async Task SetUserWithActiveSession(IUserWithActiveSessionViewRepository userWithActiveSessionViewRepository)
        {
            UserWithActiveSession.Clear();
            var userList = await userWithActiveSessionViewRepository.GetAllUsersWithActive();
            if (userList.Count > 0)
            {
                UserWithActiveSession.AddRange(userList);
            }
        }

        public async Task<string> GetLoginBySessionAsync(Guid sessionId)
        {
            var user = UserWithActiveSession.FirstOrDefault(x => x.SessionId == sessionId);
            return (user != null) ? user.Login : string.Empty;
        }

        public async Task SetActiveUserAsync(Guid sessionId, string token, string login, string role)
        {
            var user = UserWithActiveSession.FirstOrDefault(x => x.SessionId == sessionId);

            var isAddSession = false;

            if (user == null)
            {
                user = UserWithActiveSession.FirstOrDefault(x => x.Login == login);
                isAddSession = (user != null);
            }

            if (user != null)
            {
                int index = UserWithActiveSession.IndexOf(user);
                if (isAddSession)
                {
                    UserWithActiveSession[index].SessionId = sessionId;
                    UserWithActiveSession[index].Token = token;
                }
                UserWithActiveSession[index].Status = SessionStatus.Active;
                if (!string.IsNullOrWhiteSpace(role))
                {
                    UserWithActiveSession[index].SysName = role;
                }
            }
        }


        public async Task<(string, string)> GetNameAndRole(string idSession)
        {
            var activeSessionView = UserWithActiveSession.FirstOrDefault(x => x.SessionId.ToString() == idSession);
            if (activeSessionView == null)
            {
                return (string.Empty, string.Empty);
            }
            else
            {
                return (activeSessionView.UserName, activeSessionView.SysName);
            }
        }

        public async Task<bool> IsActive(string idSession)
        {
            var user = UserWithActiveSession.FirstOrDefault(x => x.SessionId.ToString() == idSession &&
                                                      x.Status == SessionStatus.Active);
            var isActive = (user != null) && !string.IsNullOrWhiteSpace(user.Token);
            return isActive;
        }

        public async Task<(string, string, string)> GetInoSession(string token)
        {
            var sessionInfo = UserWithActiveSession.FirstOrDefault(x => x.Token == token &&
                                                                        x.Status == SessionStatus.Active);
            if (sessionInfo == null)
            {
                return (null, null, null);
            }
            else
            {
                return (sessionInfo.SessionId.ToString(), sessionInfo.UserName, sessionInfo.SysName);
            }
        }
    }
}

