﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMicroService.Auth.WebAPI.IdentitySettings
{
    public interface IUpdateUserList
    {
        Task Done();
    }
}
