﻿namespace TestMicroService.Auth.WebAPI.Model
{
    public class EnumInfo
    {
        public string Title { get; set; }
        public int Code { get; set; }
    }
}
