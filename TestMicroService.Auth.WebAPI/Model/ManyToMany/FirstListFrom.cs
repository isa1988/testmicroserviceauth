﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMicroService.Auth.WebAPI.Model.ManyToMany
{
    public class FirstListFrom<T>
        where T : class
    {
        public FirstListFrom()
        {
            ValueList = new List<T>();
        }

        public FirstListFrom(List<T> value)
        {
            ValueList = value;
        }
        public List<T> ValueList { get; set; }
    }
}
