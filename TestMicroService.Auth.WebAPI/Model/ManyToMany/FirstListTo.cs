﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMicroService.Auth.WebAPI.Model.ManyToMany
{
    public class FirstListTo<T>
        where T : class
    {
        public FirstListTo()
        {
            ValueList = new List<T>();
        }

        public FirstListTo(List<T> value)
        {
            ValueList = value;
        }
        public List<T> ValueList { get; set; }
    }
}
