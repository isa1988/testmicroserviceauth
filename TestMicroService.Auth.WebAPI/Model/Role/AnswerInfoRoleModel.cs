﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMicroService.Auth.WebAPI.Model.Role
{
    public class AnswerInfoRoleModel : ErrorResponseModel
    {
        public string RoleSysName { get; set; }
    }
}
