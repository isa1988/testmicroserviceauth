﻿using System;

namespace TestMicroService.Auth.WebAPI.Model.Role
{
    public class RoleOneViewResponseModel : ErrorResponseModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
    }
}
