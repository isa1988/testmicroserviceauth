﻿using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.WebAPI.Model
{
    public class ErrorResponseModel
    {
        public ResultCode ResultCode { get; set; }
        public string ResultMessage { get; set; }
    }
}
