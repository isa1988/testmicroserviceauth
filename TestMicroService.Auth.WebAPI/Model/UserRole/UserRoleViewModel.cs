﻿using System;
using TestMicroService.Auth.WebAPI.Model.Role;
using TestMicroService.Auth.WebAPI.Model.User;

namespace TestMicroService.Auth.WebAPI.Model.UserRole
{
    public class UserRoleViewModel
    {
        public Guid Id { get; set; }
        public Guid RoleId { get; set; }
        public RoleViewModel Role { get; set; }
        public Guid UserId { get; set; }
        public UserViewModel User { get; set; }
    }
}
