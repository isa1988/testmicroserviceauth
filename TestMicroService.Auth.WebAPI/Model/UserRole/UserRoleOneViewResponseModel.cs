﻿using System;
using TestMicroService.Auth.WebAPI.Model.Role;
using TestMicroService.Auth.WebAPI.Model.User;

namespace TestMicroService.Auth.WebAPI.Model.UserRole
{
    public class UserRoleOneViewResponseModel : ErrorResponseModel
    {
        public Guid Id { get; set; }
        public RoleOneViewResponseModel Role { get; set; }
        public Guid UserId { get; set; }
        public UserOneViewResponseModel User { get; set; }
    }
}
