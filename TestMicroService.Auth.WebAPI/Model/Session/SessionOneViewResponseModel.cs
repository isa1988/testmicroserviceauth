﻿using System;
using TestMicroService.Auth.WebAPI.Model.Role;
using TestMicroService.Auth.WebAPI.Model.User;

namespace TestMicroService.Auth.WebAPI.Model.Session
{
    public class SessionOneViewResponseModel : ErrorResponseModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public UserOneViewResponseModel User { get; set; }
        public Guid RoleId { get; set; }
        public RoleOneViewResponseModel Role { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string Info { get; set; }
    }
}
