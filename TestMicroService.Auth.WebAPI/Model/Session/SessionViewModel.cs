﻿using System;
using TestMicroService.Auth.WebAPI.Model.Role;
using TestMicroService.Auth.WebAPI.Model.User;

namespace TestMicroService.Auth.WebAPI.Model.Session
{
    public class SessionViewModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public UserViewModel User { get; set; }
        public Guid RoleId { get; set; }
        public RoleViewModel Role { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string Info { get; set; }
    }
}
