﻿using System;
using System.Collections.Generic;

namespace TestMicroService.Auth.WebAPI.Model
{
    public class PageInfoListModel<T, TOrder>
        where T : class
        where TOrder : Enum
    {
        public int TotalCount { get; set; }
        public List<T> ValueList { get; set; }
        public PagingOrderSettingModel<TOrder> OrderSetting { get; set; }
    }
}

