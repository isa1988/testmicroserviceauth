﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.WebAPI.Model.Filters
{
    public class UserRoleFilterModel
    {
        public Guid UserId { get; set; }
        public PagingOrderSettingModel<OrderUserRoleFields> OrderSetting { get; set; }
    }
}
