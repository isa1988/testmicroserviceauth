﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMicroService.Auth.WebAPI.Model.Filters
{
    public class SearchByOneLineFilter<T>
        where T : Enum
    {
        public string Search { get; set; }
        public PagingOrderSettingModel<T> OrderSetting { get; set; }
    }
}
