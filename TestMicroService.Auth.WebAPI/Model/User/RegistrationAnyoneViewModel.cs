﻿namespace TestMicroService.Auth.WebAPI.Model.User
{
    public class RegistrationAnyoneViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
    }
}
