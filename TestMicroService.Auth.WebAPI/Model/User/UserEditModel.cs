﻿using System;

namespace TestMicroService.Auth.WebAPI.Model.User
{
    public class UserEditModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
    }
}
