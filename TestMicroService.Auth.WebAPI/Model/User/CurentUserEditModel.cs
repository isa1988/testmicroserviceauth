﻿using System;
using System.Text.Json.Serialization;

namespace TestMicroService.Auth.WebAPI.Model.User
{
    public class CurentUserEditModel
    {
        [JsonIgnore]
        public Guid SessionId { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
    }
}
