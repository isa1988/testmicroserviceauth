﻿using System;

namespace TestMicroService.Auth.WebAPI.Model.User
{
    public class AddRoleForUserModel
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
    }
}
