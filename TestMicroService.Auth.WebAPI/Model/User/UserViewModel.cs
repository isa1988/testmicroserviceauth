﻿using System;

namespace TestMicroService.Auth.WebAPI.Model.User
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
    }
}
