﻿using System;
using System.Text.Json.Serialization;

namespace TestMicroService.Auth.WebAPI.Model.User
{
    public class LoginConfirmRoleModel
    {
        [JsonIgnore]
        public Guid SessionId { get; set; }
        public string RoleName { get; set; }
        public string RoleSys { get; set; }
    }
}
