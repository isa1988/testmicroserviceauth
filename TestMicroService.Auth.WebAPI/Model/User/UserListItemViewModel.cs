﻿using System;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.WebAPI.Model.User
{
    public class UserListItemViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public DateTime CreateDate { get; set; }
        public string Email { get; set; }
        public DateTime? StartSessionDate { get; set; }
        public SessionStatus? Status { get; set; }
        public DateTime? LastDate { get; set; }

    }
}