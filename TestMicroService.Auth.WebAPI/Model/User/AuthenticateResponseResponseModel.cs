﻿using System.Collections.Generic;
using TestMicroService.Auth.WebAPI.Model.Role;

namespace TestMicroService.Auth.WebAPI.Model.User
{
    public class AuthenticateResponseResponseModel : ErrorResponseModel
    {
        public AuthenticateResponseResponseModel()
        {
            Roles = new List<RoleInfoModel>();
        }
        public List<RoleInfoModel> Roles { get; set; }
        public string Token { get; set; }
    }
}
