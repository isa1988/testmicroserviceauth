﻿namespace TestMicroService.Auth.WebAPI.Model.User
{
    public class UserCaptionModel
    {
        public string Title { get; set; }
    }
}
