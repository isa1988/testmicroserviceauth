﻿using System;
using System.Text.Json.Serialization;

namespace TestMicroService.Auth.WebAPI.Model.User
{
    public class CurrentUserChangePasswordModel
    {
        [JsonIgnore]
        public Guid SessionId { get; set; }
        public string PasswordCurrent { get; set; }
        public string PasswordNew { get; set; }
        public string PasswordNewConfirm { get; set; }
    }
}
