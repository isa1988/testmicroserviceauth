﻿using System;

namespace TestMicroService.Auth.WebAPI.Model.User
{
    public class UserChangePasswordModel
    {
        public Guid Id { get; set; }
        public string PasswordNew { get; set; }
        public string PasswordNewConfirm { get; set; }
    }
}
