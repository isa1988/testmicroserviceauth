﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using TestMicroService.Auth.InterfaceOfService.Contract;
using TestMicroService.Auth.WebAPI.IdentitySettings;

namespace TestMicroService.Auth.WebAPI.UserMiddleware
{
    public class AuthenticationSchemeMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IAppConfiguration appConfiguration;

        public AuthenticationSchemeMiddleware(RequestDelegate next, IAppConfiguration appConfiguration)
        {
            _next = next;
            this.appConfiguration = appConfiguration;
        }
        public async Task Invoke(HttpContext context)
        {
            if (!string.IsNullOrWhiteSpace(context.Request.Headers["AuthorizationToken"]))
            {
                var sessionAnswer =
                    await appConfiguration.GetInoSession(context.Request.Headers["AuthorizationToken"]);
                if (!string.IsNullOrWhiteSpace(sessionAnswer.Item1) &&
                    !string.IsNullOrWhiteSpace(sessionAnswer.Item2) &&
                    !string.IsNullOrWhiteSpace(sessionAnswer.Item3))
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, sessionAnswer.Item1),
                        new Claim(ClaimsIdentity.DefaultNameClaimType, sessionAnswer.Item2),
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, sessionAnswer.Item3),
                    };

                    var appIdentity = new ClaimsIdentity(claims, "Auth");
                    context.User = new ClaimsPrincipal(appIdentity);

                }
                else if (!string.IsNullOrWhiteSpace(sessionAnswer.Item1) &&
                         !string.IsNullOrWhiteSpace(sessionAnswer.Item2) &&
                         string.IsNullOrWhiteSpace(sessionAnswer.Item3))
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, sessionAnswer.Item1),
                        new Claim(ClaimsIdentity.DefaultNameClaimType, sessionAnswer.Item2),
                    };

                    var appIdentity = new ClaimsIdentity(claims, "Auth");
                    context.User = new ClaimsPrincipal(appIdentity);

                }
                else
                {
                    context.User = null;
                }

            }
            else
            {
                context.User = null;
            }

            await _next(context);
        }
    }
    public static class AuthenticationSchemeMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthenticationSchemeMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthenticationSchemeMiddleware>();
        }
    }
}
