﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TestMicroService.Auth.InterfaceOfService;
using TestMicroService.Auth.InterfaceOfService.Filters;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto.User;
using TestMicroService.Auth.Primitive;
using TestMicroService.Auth.WebAPI.Helper;
using TestMicroService.Auth.WebAPI.Model;
using TestMicroService.Auth.WebAPI.Model.Filters;
using TestMicroService.Auth.WebAPI.Model.ManyToMany;
using TestMicroService.Auth.WebAPI.Model.Role;
using TestMicroService.Auth.WebAPI.Model.Session;
using TestMicroService.Auth.WebAPI.Model.User;
using TestMicroService.Auth.WebAPI.Model.UserRole;

namespace TestMicroService.Auth.WebAPI.AppStart
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            GeneralMapping();
            UserMapping();
            RoleMapping();
            SessionMapping();
        }

        private void GeneralMapping()
        {
            CreateMap(typeof(PagingOrderSettingModel<>), typeof(PagingOrderSettingDto<>));
            CreateMap(typeof(PagingOrderSettingDto<>), typeof(PagingOrderSettingModel<>));
            CreateMap(typeof(SearchByOneLineFilter<>), typeof(SearchByOneLineDtoFilter<>));
            CreateMap(typeof(PageInfoListDto<,>), typeof(PageInfoListModel<,>));
        }

        private void SessionMapping()
        {
            CreateMap<SessionDto, SessionViewModel>();
            CreateMap<SessionDto, SessionOneViewResponseModel>();
        }
        private void RoleMapping()
        {
            CreateMap<RoleDto, RoleViewModel>()
                .ForMember(x => x.Role, m => m.MapFrom(n => n.SysName));
            CreateMap<RoleDto, RoleOneViewResponseModel>()
                .ForMember(x => x.Role, m => m.MapFrom(n => n.SysName));
            CreateMap<RoleInfoModel, RoleDto>()
                .ForMember(x => x.SysName, m => m.MapFrom(n => n.Role));
            CreateMap<RoleDto, RoleInfoModel>()
                .ForMember(x => x.Role, m => m.MapFrom(n => n.SysName));

            CreateMap<UserRoleFilterModel, UserRoleDtoFilter>();

            CreateMap<UserRoleDto, UserRoleViewModel>();
            CreateMap<UserRoleDto, UserRoleOneViewResponseModel>();
        }

        private void UserMapping()
        {

            CreateMap<CurentUserEditModel, UserEditDto>()
                .ForMember(x => x.Id, p => p.MapFrom(m => m.SessionId));
            CreateMap<UserEditModel, UserEditDto>();

            CreateMap<AddRoleForUserModel, UserRoleDto>()
                .ForMember(x => x.Id, p => p.Ignore())
                .ForMember(x => x.User, p => p.Ignore())
                .ForMember(x => x.Role, p => p.Ignore());

            CreateMap<CurrentUserChangePasswordModel, UserChangePasswordDto>()
                .ForMember(x => x.Id, p => p.MapFrom(m => m.SessionId))
                .ForMember(x => x.IsCheck, p => p.MapFrom(n => true));
            CreateMap<UserChangePasswordModel, UserChangePasswordDto>()
                .ForMember(x => x.PasswordCurrent, p => p.Ignore())
                .ForMember(x => x.IsCheck, p => p.MapFrom(n => false));

            CreateMap<UserCaptionDto, UserCaptionModel>();

            CreateMap<UserDto, UserOneViewResponseModel>();
            CreateMap<UserDto, UserViewModel>();

            CreateMap<AuthenticateResponseDto, AuthenticateResponseResponseModel>();
            CreateMap<LoginModel, LoginDto>();
            CreateMap<LoginConfirmRoleModel, LoginConfirmRoleDto>();


            CreateMap<RoleFilterModel, RoleDtoFilter>();

            CreateMap<RegistrationAnyoneViewModel, RegistrationDto>()
                .ForMember(x => x.RoleList, p => p.Ignore());

            CreateMap<(FirstListFrom<UserDto>, FirstListFrom<SessionDto>), FirstListTo<UserListItemViewModel>>().ForMember(x => x.ValueList,
                p => p.MapFrom(m =>
                    m.Item1.ValueList.LeftOuterJoin(
                        m.Item2.ValueList,
                        user => user.Id,
                        session => session.UserId,
                        (user, session) => 
                            new UserListItemViewModel
                            {
                            Id = user.Id,
                            Email = user.Email,
                            Name = user.Name,
                            StartSessionDate = (session != null) ? (DateTime?)(session.CreateDate) : null,
                            LastDate = (session != null) ? session.CloseDate : null,
                            Status = (user == null || session == null) ? SessionStatus.Closed : session.Status
                        })
                ));
        }
    }
}
