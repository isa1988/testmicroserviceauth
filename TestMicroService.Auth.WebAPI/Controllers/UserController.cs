﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using TestMicroService.Auth.InterfaceOfService;
using TestMicroService.Auth.InterfaceOfService.Contract;
using TestMicroService.Auth.InterfaceOfService.Filters;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto.User;
using TestMicroService.Auth.Primitive;
using TestMicroService.Auth.Primitive.Order;
using TestMicroService.Auth.WebAPI.Helper;
using TestMicroService.Auth.WebAPI.IdentitySettings;
using TestMicroService.Auth.WebAPI.Model;
using TestMicroService.Auth.WebAPI.Model.Filters;
using TestMicroService.Auth.WebAPI.Model.ManyToMany;
using TestMicroService.Auth.WebAPI.Model.Role;
using TestMicroService.Auth.WebAPI.Model.Session;
using TestMicroService.Auth.WebAPI.Model.User;
using TestMicroService.Auth.WebAPI.Model.UserRole;

namespace TestMicroService.Auth.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public UserController(IMapper mapper,
            IConfiguration configuration,
            IRoleService roleService,
            IUserService userService,
            IUserRoleService userRoleService,
            ISessionService sessionService,
            IAppConfiguration appConfiguration,
            IOptions<JwtSetting> fileSetting)
        {
            this.mapper = mapper;
            this.configuration = configuration;
            this.roleService = roleService;
            this.userService = userService;
            this.userRoleService = userRoleService;
            this.sessionService = sessionService;
            this.appConfiguration = appConfiguration;
            this.fileSetting = fileSetting;
        }

        private readonly IMapper mapper;
        private readonly IConfiguration configuration;
        private readonly IRoleService roleService;
        private readonly IUserService userService;
        private readonly IUserRoleService userRoleService;
        private readonly ISessionService sessionService;
        private readonly IAppConfiguration appConfiguration;
        private readonly IOptions<JwtSetting> fileSetting;

        /// <summary>
        /// Поля для сортировки пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet("getInfoOrderUser")]
        public async Task<List<EnumInfo>> GetInfoOrderUserAsync()
        {
            var retList = await GetInfoOfEnumAsync<OrderUserFields>();

            return retList;
        }

        /// <summary>
        /// Вытащить всех пользователей
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageInfoListModel<UserListItemViewModel, OrderUserFields>> GetAllAsync(
            PagingOrderSettingModel<OrderUserFields> orderSettingModel)
        {
            orderSettingModel = DefaultOrder(orderSettingModel);
            var orderSet = mapper.Map<PagingOrderSettingDto<OrderUserFields>>(orderSettingModel);
            var dto = await userService.GetAllDetailsAsync(orderSet);
            if (dto.ValueList == null || dto.ValueList.Count == 0)
            {
                var modelRet = mapper.Map<PageInfoListModel<UserListItemViewModel, OrderUserFields>>(dto);
                return modelRet;
            }

            var userIdList = dto.ValueList.Select(x => x.Id).ToList();
            var activeList = await sessionService.GetActiveByUserListAsync(userIdList);

            var userModel = mapper.Map<FirstListTo<UserListItemViewModel>>((
                new FirstListFrom<UserDto>(dto.ValueList),
                new FirstListFrom<SessionDto>(activeList)));

            var model = new PageInfoListModel<UserListItemViewModel, OrderUserFields>()
            {
                ValueList = userModel.ValueList,
                OrderSetting = mapper.Map<PagingOrderSettingModel<OrderUserFields>>(dto.OrderSetting),
                TotalCount = dto.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Поиск
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("search")]
        public async Task<PageInfoListModel<UserListItemViewModel, OrderUserFields>> GetAllFromSearchAsync(
            SearchByOneLineFilter<OrderUserFields> filter)
        {
            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            var orderSet = mapper.Map<SearchByOneLineDtoFilter<OrderUserFields>>(filter);
            var dto = await userService.GetAllFromSearchAsync(orderSet);
            if (dto.ValueList == null || dto.ValueList.Count == 0)
            {
                var modelRet = mapper.Map<PageInfoListModel<UserListItemViewModel, OrderUserFields>>(dto);
                return modelRet;
            }

            var userIdList = dto.ValueList.Select(x => x.Id).ToList();
            var activeList = await sessionService.GetActiveByUserListAsync(userIdList);

            var userModel = mapper.Map<FirstListTo<UserListItemViewModel>>((
                new FirstListFrom<UserDto>(dto.ValueList),
                new FirstListFrom<SessionDto>(activeList)));

            var model = new PageInfoListModel<UserListItemViewModel, OrderUserFields>()
            {
                ValueList = userModel.ValueList,
                OrderSetting = mapper.Map<PagingOrderSettingModel<OrderUserFields>>(dto.OrderSetting),
                TotalCount = dto.TotalCount
            };

            return model;
        }


        /// <summary>
        /// Показать пользователя по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<UserOneViewResponseModel> GetByIdAsync(Guid id)
        {
            var dto = await userService.GetByIdDeteilsAsync(id);
            if (dto.IsSuccess)
            {
                var model = mapper.Map<UserOneViewResponseModel>(dto.Entity);
                model.ResultCode = ResultCode.Success;
                return model;
            }
            else
            {
                return new UserOneViewResponseModel()
                {
                    ResultCode = ResultCode.Error,
                    ResultMessage = dto.GetErrorString()
                };
            }
        }

        /// <summary>
        /// Показать информацию о текущем пользователе
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getInfoCurrentUser")]
        public async Task<UserOneViewResponseModel> GetInfoCurrentUser(Guid sessionId)
        {
            var userId = await userService.GetUserIdBySessionAsync(sessionId);
            var user = await GetByIdAsync(userId);
            return user;
        }

        /// <summary>
        /// Авторизация
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<AuthenticateResponseResponseModel> LoginAsync(LoginModel request)
        {
            var dto = mapper.Map<LoginDto>(request);
            var response = await userService.LoginAsync(dto, fileSetting.Value.Key, fileSetting.Value.Issuer);
            if (response.IsSuccess)
            {
                var model = mapper.Map<AuthenticateResponseResponseModel>(response.Entity);
                model.ResultCode = ResultCode.Success;
                if (model.Roles.Count == 1)
                {
                    await appConfiguration.SetActiveUserAsync(response.Entity.SessionId, response.Entity.Token, request.Login, model.Roles[0].Role);
                }
                else
                {
                    await appConfiguration.SetActiveUserAsync(response.Entity.SessionId, response.Entity.Token, request.Login, string.Empty);
                }
                model.Roles = new List<RoleInfoModel>();

                return model;
            }
            else
            {
                return new AuthenticateResponseResponseModel
                    {ResultCode = ResultCode.Error, ResultMessage = response.GetErrorString()};
            }
        }

        /// <summary>
        /// Подтвеждеие роли, если у пользователя больше одной
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        [Authorize]
        [HttpPost("rolesselect")]
        public async Task<ErrorResponseModel> RoleSelectAsync(LoginConfirmRoleModel request)
        {
            var dto = mapper.Map<LoginConfirmRoleDto>(request);
            var response = await userService.RoleSelectAsync(dto);
            if (response.IsSuccess)
            {
                var login = await appConfiguration.GetLoginBySessionAsync(Guid.Parse(User.Identity.Name));
                if (!string.IsNullOrWhiteSpace(login))
                {
                    await appConfiguration.SetActiveUserAsync(response.Entity.Id, string.Empty, login, request.RoleSys);
                }
                return new ErrorResponseModel {ResultCode = ResultCode.Success};
            }
            else
            {
                return new ErrorResponseModel
                    {ResultCode = ResultCode.Error, ResultMessage = response.GetErrorString()};
            }
        }

        /// <summary>
        /// Регистрация любого пользователя
        /// </summary>
        /// <param name="registrationModel"></param>
        /// <returns></returns>
        [HttpPost("registrationGeneral")]
        public async Task<ErrorResponseModel> RegistrationGeneralAsync(RegistrationAnyoneViewModel registrationModel)
        {
            var dto = mapper.Map<RegistrationDto>(registrationModel);
            var result = await userService.RegistrationGeneralAsync(dto);
            if (result.IsSuccess)
            {
                return new ErrorResponseModel
                {
                    ResultCode = ResultCode.Success
                };
            }
            else
            {
                return new ErrorResponseModel()
                {
                    ResultCode = ResultCode.Error,
                    ResultMessage = result.GetErrorString()
                };
            }
        }

        /*/// <summary>
        /// Редакировать пользователя
        /// </summary>
        /// <param name="editModel"></param>
        /// <returns></returns>
        [HttpPut("edit")]
        public async Task<ErrorResponseModel> EditAsync(UserEditModel editModel)
        {
            var dto = mapper.Map<UserEditDto>(editModel);
            var result = await userService.EditAsync(dto);
            if (result.IsSuccess)
            {
                return new ErrorResponseModel
                {
                    ResultCode = ResultCode.Success
                };
            }
            else
            {
                return new ErrorResponseModel()
                {
                    ResultCode = ResultCode.Error,
                    ResultMessage = result.GetErrorString()
                };
            }
        }

        /// <summary>
        /// Редакировать текущего пользователя
        /// </summary>
        /// <param name="editModel"></param>
        /// <returns></returns>
        [HttpPut("editCurrent")]
        public async Task<ErrorResponseModel> EditCurrentAsync(CurentUserEditModel editModel)
        {
            var dto = mapper.Map<UserEditDto>(editModel);
            var result = await userService.EditCurrentAsync(dto);
            if (result.IsSuccess)
            {
                return new ErrorResponseModel
                {
                    ResultCode = ResultCode.Success
                };
            }
            else
            {
                return new ErrorResponseModel()
                {
                    ResultCode = ResultCode.Error,
                    ResultMessage = result.GetErrorString()
                };
            }
        }

        /// <summary>
        /// Поменять пароль
        /// </summary>
        /// <param name="changePasswordModel"></param>
        /// <returns></returns>
        [HttpPut("changePassword")]
        public async Task<ErrorResponseModel> ChangePasswordAsync(UserChangePasswordModel changePasswordModel)
        {
            var dto = mapper.Map<UserChangePasswordDto>(changePasswordModel);
            var result = await userService.ChangePasswordAsync(dto);
            if (result.IsSuccess)
            {
                return new ErrorResponseModel
                {
                    ResultCode = ResultCode.Success
                };
            }
            else
            {
                return new ErrorResponseModel()
                {
                    ResultCode = ResultCode.Error,
                    ResultMessage = result.GetErrorString()
                };
            }
        }

        /// <summary>
        /// Поменять пароль у текущего пользоватля
        /// </summary>
        /// <param name="changePasswordModel"></param>
        /// <returns></returns>
        [HttpPut("changePasswordCurrent")]
        public async Task<ErrorResponseModel> ChangePasswordCurrentAsync(
            CurrentUserChangePasswordModel changePasswordModel)
        {
            var dto = mapper.Map<UserChangePasswordDto>(changePasswordModel);
            var result = await userService.ChangePasswordCurrentAsync(dto);
            if (result.IsSuccess)
            {
                return new ErrorResponseModel
                {
                    ResultCode = ResultCode.Success
                };
            }
            else
            {
                return new ErrorResponseModel()
                {
                    ResultCode = ResultCode.Error,
                    ResultMessage = result.GetErrorString()
                };
            }
        }*/

        /// <summary>
        /// Удалить пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete{id:guid}")]
        public async Task<ErrorResponseModel> DeleteAsync(Guid id)
        {
            var result = await userService.DeleteItemAsync(id);
            if (result.IsSuccess)
            {
                return new ErrorResponseModel
                {
                    ResultCode = ResultCode.Success
                };
            }
            else
            {
                return new ErrorResponseModel()
                {
                    ResultCode = ResultCode.Error,
                    ResultMessage = result.GetErrorString()
                };
            }
        }

        /*/// <summary>
        /// Поля для сортировки пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet("getInfoOrderRole")]
        public async Task<List<EnumInfo>> GetInfoOrderRoleAsync()
        {
            var retList = await GetInfoOfEnumAsync<OrderRoleFields>();

            return retList;
        }

        /// <summary>
        /// Вернуть все роли
        /// </summary>
        /// <param name="orderSettingModel"></param>
        /// <returns></returns>
        [HttpPost("allRole")]
        public async Task<PageInfoListModel<RoleInfoModel, OrderRoleFields>> GetAllRoleListAsync(
            PagingOrderSettingModel<OrderRoleFields> orderSettingModel)
        {
            orderSettingModel = DefaultOrder(orderSettingModel);
            var orderSet = mapper.Map<PagingOrderSettingDto<OrderRoleFields>>(orderSettingModel);
            var roleDtoList = await roleService.GetAllDetailsAsync(orderSet);
            var model = mapper.Map<PageInfoListModel<RoleInfoModel, OrderRoleFields>>(roleDtoList);
            return model;
        }

        /// <summary>
        /// Вернуть список не заполненных ролей для пользователя
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("getRoleExcludeUserRole")]
        public async Task<PageInfoListModel<RoleInfoModel, OrderRoleFields>> GetRoleExcludeUserRoleAsync(
            RoleFilterModel filter)
        {
            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            var orderSet = mapper.Map<RoleDtoFilter>(filter);
            var dto = await userService.GetRoleExcludeUserRoleAsync(orderSet);

            var model = mapper.Map<PageInfoListModel<RoleInfoModel, OrderRoleFields>>(dto);

            return model;
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="addRoleForUserModel"></param>
        /// <returns></returns>
        [HttpPut("addRole")]
        public async Task<ErrorResponseModel> AddRoleForUserAsync(AddRoleForUserModel addRoleForUserModel)
        {
            var dto = mapper.Map<UserRoleDto>(addRoleForUserModel);
            var result = await userRoleService.CreateAsync(dto);
            if (result.IsSuccess)
            {
                return new ErrorResponseModel
                {
                    ResultCode = ResultCode.Success
                };
            }
            else
            {
                return new ErrorResponseModel()
                {
                    ResultCode = ResultCode.Error,
                    ResultMessage = result.GetErrorString()
                };
            }
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="userRoleId"></param>
        /// <returns></returns>
        [HttpDelete("deleteRole")]
        public async Task<ErrorResponseModel> DeleteRoleForUserAsync(Guid userRoleId)
        {
            var result = await userRoleService.DeleteItemAsync(userRoleId);
            if (result.IsSuccess)
            {
                return new ErrorResponseModel
                {
                    ResultCode = ResultCode.Success
                };
            }
            else
            {
                return new ErrorResponseModel()
                {
                    ResultCode = ResultCode.Error,
                    ResultMessage = result.GetErrorString()
                };
            }
        }

        /// <summary>
        /// Все роли для текущего пользователя
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("getAllRoleForUser")]
        public async Task<PageInfoListModel<UserRoleViewModel, OrderUserRoleFields>> GetAllForUserAsync(
            UserRoleFilterModel filter)
        {
            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            var filterSet = mapper.Map<UserRoleDtoFilter>(filter);
            var dto = await userRoleService.GetAllForUserAsync(filterSet);
            var model = mapper.Map<PageInfoListModel<UserRoleViewModel, OrderUserRoleFields>>(dto);
            return model;
        }

        /// <summary>
        /// Вернуть приветствие
        /// </summary>
        /// <returns></returns>
        [HttpGet("getUserCaption")]
        public async Task<UserCaptionModel> GetUserCaptionAsync()
        {
            Guid sessionId = Guid.Parse(User.Identity.Name);
            var dto = await userService.GetUserCaptionAsync(sessionId);
            var model = new UserCaptionModel();

            if (dto.IsSuccess)
            {
                model = mapper.Map<UserCaptionModel>(dto.Entity);
            }

            return model;
        }*/

        [HttpGet("checkToken/{token}")]
        public async Task<AnswerInfoRoleModel> CheckTokenAsync(string token)
        {
            var session = await appConfiguration.GetInoSession(token);
            if (string.IsNullOrWhiteSpace(session.Item3))
            {
                return new AnswerInfoRoleModel
                {
                    ResultMessage = "Роль не найдена",
                    ResultCode = ResultCode.Error,
                };
            }
            else
            {
                return new AnswerInfoRoleModel
                {
                    RoleSysName = session.Item3,
                    ResultCode = ResultCode.Success,
                };
            }
        }

        private PagingOrderSettingModel<TOrder> DefaultOrder<TOrder>(
            PagingOrderSettingModel<TOrder> orderSettingModel)
            where TOrder : Enum
        {
            if (orderSettingModel == null || orderSettingModel.PageSize == 0)
            {
                return orderSettingModel = new PagingOrderSettingModel<TOrder>
                {
                    StartPosition = 0,
                    PageSize = 20
                };
            }
            else
            {
                return orderSettingModel;
            }
        }

        private async Task<List<EnumInfo>> GetInfoOfEnumAsync<T>()
            where T : Enum
        {
            var values = Enum.GetValues(typeof(T));
            var retList = new List<EnumInfo>();
            foreach (var value in values)
            {
                retList.Add(new EnumInfo
                {
                    Title = ((T) value).GetEnumDisplayName(),
                    Code = (int) value,
                });
            }

            return retList;
        }
    }
}


