﻿using System;

namespace TestMicroService.Auth.InterfaceOfService
{
    public interface IServiceDto
    {
    }
    public interface IServiceDto<TId> : IServiceDto 
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
