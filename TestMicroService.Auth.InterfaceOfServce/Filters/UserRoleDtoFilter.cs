﻿using System;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.InterfaceOfService.Filters
{
    public class UserRoleDtoFilter
    {
        public Guid UserId { get; set; }
        public PagingOrderSettingDto<OrderUserRoleFields> OrderSetting { get; set; }
    }
}
