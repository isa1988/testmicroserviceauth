﻿using System;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.InterfaceOfService.Filters
{
    public class RoleDtoFilter
    {
        public Guid UserId { get; set; }
        public PagingOrderSettingDto<OrderRoleFields> OrderSetting { get; set; }
    }
}
