﻿using System;

namespace TestMicroService.Auth.InterfaceOfService.Filters
{
    public class SearchByOneLineDtoFilter<T>
    where T: Enum
    {
        public string Search { get; set; }
        public PagingOrderSettingDto<T> OrderSetting { get; set; }
    }
}
