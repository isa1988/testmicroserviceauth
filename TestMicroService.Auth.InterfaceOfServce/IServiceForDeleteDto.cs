﻿using System;

namespace TestMicroService.Auth.InterfaceOfService
{
    public interface IServiceForDeleteDto : IServiceDto
    {
    }
    public interface IServiceForDeleteDto<TId> : IServiceForDeleteDto
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
