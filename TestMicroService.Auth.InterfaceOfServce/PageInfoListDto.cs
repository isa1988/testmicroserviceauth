﻿using System;
using System.Collections.Generic;

namespace TestMicroService.Auth.InterfaceOfService
{
    public class PageInfoListDto<T, TOrder>
        where T : class
        where TOrder : Enum
    {
        public int TotalCount { get; set; }
        public List<T> ValueList { get; set; }
        public PagingOrderSettingDto<TOrder> OrderSetting { get; set; }
    }
}
