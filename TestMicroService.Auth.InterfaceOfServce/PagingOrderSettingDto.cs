﻿using System;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.InterfaceOfService
{
    public class PagingOrderSettingDto<T>
        where T : Enum
    {
        public int StartPosition { get; set; }
        public int PageSize { get; set; }
        public bool IsOrder { get; set; }
        public T OrderField { get; set; }
        public OrderType OrderDirection { get; set; }
    }
}
