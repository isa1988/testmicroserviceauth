﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.InterfaceOfService.Contract
{
    public interface ISessionService : IGeneralService<SessionDto, Guid, OrderSessionFields>
    {
        /// <summary>
        /// Активная сессия
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<SessionDto>> GetActiveByIdAsync(Guid id);

        /// <summary>
        /// Активная сессия со всеми зависимостями
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<SessionDto>> GetActiveByIdDetailsAsync(Guid id);

        /// <summary>
        /// Активная сессия
        /// </summary>
        /// <param name="token">Токен</param>
        /// <returns></returns>
        
        Task<EntityOperationResult<SessionDto>> GetActiveByTokenAsync(string token);
        /// <summary>
        /// Активная сессия со всеми зависимостями
        /// </summary>
        /// <param name="token">Токен</param>
        /// <returns></returns>
        Task<EntityOperationResult<SessionDto>> GetActiveByTokenDetailAsync(string token);

        /// <summary>
        /// Вернуть активный ли пользователь
        /// </summary>
        /// <param name="userListId"></param>
        /// <returns></returns>
        Task<List<SessionDto>> GetActiveByUserListAsync(List<Guid> userListId);

    }
}
