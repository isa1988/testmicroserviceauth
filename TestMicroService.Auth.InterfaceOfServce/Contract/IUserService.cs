﻿using System;
using System.Threading.Tasks;
using TestMicroService.Auth.InterfaceOfService.Filters;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto.User;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.InterfaceOfService.Contract
{
    public interface IUserService : IGeneralServiceFromDelete<UserDto, Guid, OrderUserFields>
    {
        Task<PageInfoListDto<UserDto, OrderUserFields>> GetAllFromSearchAsync(SearchByOneLineDtoFilter<OrderUserFields> filter);
        Task<PageInfoListDto<UserDto, OrderUserFields>> GetAllFromSearchDetailAsync(SearchByOneLineDtoFilter<OrderUserFields> filter);
        Task<EntityOperationResult<AuthenticateResponseDto>> LoginAsync(LoginDto model, string key, string issuer);
        Task<EntityOperationResult<SessionDto>> RoleSelectAsync(LoginConfirmRoleDto model);
        Task<EntityOperationResult<UserDto>> RegistrationGeneralAsync(RegistrationDto registrationDto);
        Task<EntityOperationResult<UserDto>> EditAsync(UserEditDto editDto);
        Task<EntityOperationResult<UserDto>> EditCurrentAsync(UserEditDto editDto);
        Task<EntityOperationResult<UserDto>> ChangePasswordAsync(UserChangePasswordDto changePasswordDto);
        Task<EntityOperationResult<UserDto>> ChangePasswordCurrentAsync(UserChangePasswordDto changePasswordDto);
        Task<EntityOperationResult<UserCaptionDto>> GetUserCaptionAsync(Guid sessionId);
        Task<PageInfoListDto<RoleDto, OrderRoleFields>> GetRoleExcludeUserRoleAsync(RoleDtoFilter filter);
        Task<Guid> GetUserIdBySessionAsync(Guid sessionId);
    }
}
