﻿using System;
using System.Threading.Tasks;
using TestMicroService.Auth.InterfaceOfService.Filters;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.InterfaceOfService.Contract
{
    public interface IUserRoleService : IGeneralServiceFromDelete<UserRoleDto, Guid, OrderUserRoleFields>
    {
        Task<PageInfoListDto<UserRoleDto, OrderUserRoleFields>> GetAllForUserAsync(UserRoleDtoFilter filter);
    }
}
