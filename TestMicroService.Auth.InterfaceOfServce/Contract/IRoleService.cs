﻿using TestMicroService.Auth.InterfaceOfService.ModelOfDto;
using TestMicroService.Auth.Primitive.Order;

namespace TestMicroService.Auth.InterfaceOfService.Contract
{
    public interface IRoleService : IGeneralServiceFromDelete<RoleDto, OrderRoleFields>
    {
    }
}
