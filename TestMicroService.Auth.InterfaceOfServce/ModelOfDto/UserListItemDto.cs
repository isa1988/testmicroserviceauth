﻿using System;

namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto
{
    public class UserListItemDto : IServiceDto<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

    }
}