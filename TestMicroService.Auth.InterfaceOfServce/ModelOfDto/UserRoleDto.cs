﻿using System;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto.User;

namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto
{
    public class UserRoleDto : IServiceForDeleteDto<Guid>
    {
        public Guid Id { get; set; }
        public Guid RoleId { get; set; }
        public RoleDto Role { get; set; }
        public Guid UserId { get; set; }
        public UserDto User { get; set; }
    }
}
