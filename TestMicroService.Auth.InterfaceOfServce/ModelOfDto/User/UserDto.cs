﻿using System;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto.User
{
    public class UserDto : IServiceForDeleteDto<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Info { get; set; }
        public Deleted IsDeleted { get; set; }
    }
}
