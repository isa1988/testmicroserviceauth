﻿namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto.User
{
    public class UserCaptionDto : IServiceDto
    {
        public string Title { get; set; }
    }
}
