﻿using System;
using System.Collections.Generic;

namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto.User
{
    public class AuthenticateResponseDto : IServiceDto
    {
        public AuthenticateResponseDto()
        {
            Roles = new List<RoleDto>();
        }
        public Guid SessionId { get; set; }
        public List<RoleDto> Roles { get; set; }
        public string Token { get; set; }
    }
}
