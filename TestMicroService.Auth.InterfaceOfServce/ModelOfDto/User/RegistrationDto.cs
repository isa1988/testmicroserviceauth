﻿using System;
using System.Collections.Generic;

namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto.User
{
    public class RegistrationDto : IServiceDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Phone { get; set; }
        public string Info { get; set; }
        public List<RoleDto> RoleList { get; set; }
    }
}
