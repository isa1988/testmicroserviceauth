﻿namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto.User
{
    public class LoginDto : IServiceDto
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}
