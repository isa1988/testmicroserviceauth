﻿using System;

namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto.User
{
    public class UserEditDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
