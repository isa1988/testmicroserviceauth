﻿using System;

namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto.User
{
    public class LoginConfirmRoleDto
    {
        public Guid SessionId { get; set; }
        public string RoleSys { get; set; }
    }
}
