﻿using System;

namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto.User
{
    public class UserChangePasswordDto : IServiceDto<Guid>
    {
        public Guid Id { get; set; }
        public bool IsCheck { get; set; }
        public string PasswordCurrent { get; set; }
        public string PasswordNew { get; set; }
        public string PasswordNewConfirm { get; set; }
    }
}
