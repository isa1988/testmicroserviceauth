﻿using System;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto
{
    public class RoleDto : IServiceForDeleteDto<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SysName { get; set; }
        public Deleted IsDeleted { get; set; }
    }
}
