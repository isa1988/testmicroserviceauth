﻿using System;
using TestMicroService.Auth.InterfaceOfService.ModelOfDto.User;
using TestMicroService.Auth.Primitive;

namespace TestMicroService.Auth.InterfaceOfService.ModelOfDto
{
    public class SessionDto : IServiceDto<Guid>
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public UserDto User { get; set; }
        public Guid RoleId { get; set; }
        public UserRoleDto UserRole { get; set; }
        public SessionStatus Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string Info { get; set; }
    }
}
